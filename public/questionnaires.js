var Question = function(identifier) {
    this.identifier = identifier;

    this.isVisible = function isVisible(answers) {
        var visible = true;

        var dependencies = this.getDependencies();
        if (dependencies != null) {
            for (index in dependencies) {
                var dependency = dependencies[index];
                if (dependency.identifier in answers) {
                    var answerDepended = answers[dependency.identifier];
                } else {
                    var answerDepended = null;
                }
                if (!this.isDependencyMet(dependency, answerDepended)) {
                    visible = false;
                }
            }
        }

        return visible;
    },
            this.isDependencyMet = function(dependency, value) {
                if (dependency.dependendValue != value) {
                    return false;
                }
                return true;
            },
            this.getDependencies = function() {
                if (coco.dependencies[this.identifier]) {
                    return coco.dependencies[this.identifier];
                }
                return null;
            }
};

var QuestionCollection = function() {

    this.getAnswers = function() {

        var tmpAnswers = $('#questionnaire_form').serializeArray();
        var answers = [];
        tmpAnswers.forEach(function(answer) {
            answers[answer.name] = answer.value;
        });
        return answers;
    },
            this.updateVisibility = function() {
                var answers = this.getAnswers();
                $('.question').each(function(index, q) {
                    q = $(q);
                    var identifier = q.attr('id');
                    var escapedIdentifier = identifier.replace(/(:|\.|\[|\]|,)/g, "\\$1");
                    question = new Question(identifier.replace("question_", ""));
                    if (question.isVisible(answers)) {
                        $('#' + escapedIdentifier).removeClass('question_hidden');
                    } else {
                        $('#' + escapedIdentifier).addClass('question_hidden');
                    }
                });
                $(window).trigger('resize');
            }
}
collection = new QuestionCollection();