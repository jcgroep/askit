<?php

namespace spec\Jcgroep\Askit;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Jcgroep\Askit;

class QuestionnaireWrapperBaseSpec extends ObjectBehavior {

    function let() {
        $this->beConstructedWith([]);
    }

    function it_is_initializable() {
        $this->shouldHaveType('Jcgroep\Askit\QuestionnaireWrapperBase');
    }

    function it_adds_a_question() {
        $question = new \Jcgroep\Askit\QuestionBase("phpspec::1", "question text");
        $this->beConstructedWith([$question]);
        $this->countQuestions()->shouldReturn(1);
    }
    
    function it_returns_a_question() {
        $question = new \Jcgroep\Askit\QuestionBase("phpspec::1", "question text");
        $this->beConstructedWith([$question]);
        $this->getQuestion("phpspec::1")->shouldEqual($question);
    }
    
    function it_returns_false_on_a_unfound_question() {
        $this->getQuestion("phpspec::1")->shouldEqual(false);
    }

    function it_stores_an_answer() {
        $question = new \Jcgroep\Askit\QuestionBase("phpspec::1", "question text");
        $this->beConstructedWith([$question]);
        $this->addAnswer("phpspec::1", 1);
        $this->hasAnswer("phpspec::1")->shouldReturn(true);
        $this->hasAnswer($question)->shouldReturn(true);
        $this->countAnswered()->shouldReturn(1);
    }
    
    function it_stores_multiple_answers() {
        $question1 = new \Jcgroep\Askit\QuestionBase("phpspec::1", "question text");
        $question2 = new \Jcgroep\Askit\QuestionBase("phpspec::2", "question text");
        $this->beConstructedWith([$question1, $question2]);
        $this->addAnswers(["phpspec::1" => 1, "phpspec::2" => 1]);
        $this->countAnswered()->shouldReturn(2);
    }
    
    function it_returns_an_answer() {
        $question = new \Jcgroep\Askit\QuestionBase("phpspec::1", "question text");
        $this->beConstructedWith([$question]);
        $this->addAnswer("phpspec::1", 1);
        $this->getAnswer("phpspec::1")->shouldReturn(1);
        $this->getAnswer($question)->shouldReturn(1);
    }
    
    function it_returns_default_on_a_unfound_answer() {
        $this->getAnswer("phpspec::999", "default")->shouldEqual("default");
    }
    
    function it_validates_correctly() {
        /*
        $question = new QuestionBase("phpspec::1", "question text", ['validate' => 'integer']);
        $this->beConstructedWith([$question]);
        $this->isValid()->shouldEqual(false);
        $this->addAnswer("phpspec::1", 1);
        $this->isValid()->shouldEqual(true);
         */
    }
    
    function it_stores_readonly_attribute() {
        $this->setReadonly(true);
        $this->isReadonly()->shouldEqual(true);
    }

}
