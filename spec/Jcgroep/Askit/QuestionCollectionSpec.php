<?php

namespace spec\Jcgroep\Askit;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Jcgroep\Askit;

class QuestionCollectionSpec extends ObjectBehavior {

    function let() {
    }

    function it_is_initializable() {
        $this->shouldHaveType('Jcgroep\Askit\QuestionCollection');
    }
    
    function it_can_build_a_text_question() {
        $this->text("phpspec::1", "question text")->shouldReturnAnInstanceOf("Jcgroep\Askit\QuestionBase");
    }
    
    function it_can_build_a_multi_question() {
        $this->multi("phpspec::1", "question text")->shouldReturnAnInstanceOf("Jcgroep\Askit\QuestionMulti");
    }
    
    function it_can_build_a_bool_question() {
        $this->bool("phpspec::1", "question text")->shouldReturnAnInstanceOf("Jcgroep\Askit\QuestionBool");
    }
    
    function it_can_build_a_radio_question() {
        $this->radio("phpspec::1", "question text")->shouldReturnAnInstanceOf("Jcgroep\Askit\QuestionRadio");
    }
    
    function it_can_build_a_scale_question() {
        $this->scale("phpspec::1", "question text")->shouldReturnAnInstanceOf("Jcgroep\Askit\QuestionScale");
    }
    
    function it_can_build_a_note_question() {
        $this->note("phpspec::1", "question text")->shouldReturnAnInstanceOf("Jcgroep\Askit\QuestionNote");
    }
    
    function it_can_build_a_textarea_question() {
        $this->textarea("phpspec::1", "question text")->shouldReturnAnInstanceOf("Jcgroep\Askit\QuestionTextarea");
    }
    
    function it_can_collect_dependencies() {
        $this[] = $this->text("phpspec::1", "question text")
                ->dependsOn("phpspec::0",1);
        $this[] = $this->text("phpspec::2", "question text")
                ->dependsOn("phpspec::0",1);
        $this->getDependencies()->shouldHaveCount(2);
    }

}
