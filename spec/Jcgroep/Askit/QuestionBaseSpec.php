<?php

namespace spec\Jcgroep\Askit;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Jcgroep\Askit;

class QuestionBaseSpec extends ObjectBehavior
{
    function let()
    {
        $options = ['number' => 1, 
            'description' => 'description',
            'validators' => 'integer',
            'postfix' => 'postfix',
            'size' => 10];
        $this->beConstructedWith('phpspec::1', 'question base', $options);
    }
    
    function it_is_initializable()
    {
        $this->shouldHaveType('Jcgroep\Askit\QuestionBase');
    }
    
    function it_stores_an_identifier() {
        $this->getIdentifier()->shouldReturn('phpspec::1');
    }
    
    function it_stores_a_question_text() {
        $this->getQuestiontext()->shouldReturn('question base');
    }
    
    function it_stores_a_questionnumber_attribute() {
        $this->getNumber()->shouldReturn(1);
    }
    
    function it_stores_a_description_attribute() {
        $this->getDescription()->shouldReturn('description');
    }
    
    function it_stores_validators_attribute() {
        $this->getValidators()->shouldReturn('integer');
    }
    
    function it_stores_a_postfix_attribute() {
        $this->getPostfix()->shouldReturn('postfix');
    }
    
    function it_stores_a_size_attribute() {
        $this->getSize()->shouldReturn(10);
    }
    
    function it_stores_a_validator_attribute() {
        $this->setValidators("required");
        $this->getValidators()->shouldReturn("required");
    }
    
    function it_stores_dependences() {
        $this->dependsOn("phpspec::0", 2);
        $this->hasDependencies()->shouldReturn(true);
        $this->getDependencies()->shouldHaveCount(1);
    }
    
    function it_checks_dependencies() {
        $this->dependsOn("phpspec::0", 2);
        $this->isVisible(['phpspec::0' => 0])->shouldReturn(false);
        $this->isVisible(['phpspec::1' => 0])->shouldReturn(false);
        $this->isVisible(['phpspec::0' => 2])->shouldReturn(true);
    }
    
    function it_checks_required_correctly() {
        $this->isRequired()->shouldReturn(false);
        $this->setValidators("required");
        $this->isRequired()->shouldReturn(true);
    }
    
    function it_renders_input_without_answer() {
        // @bug juiste laravel includes kunnen doen
        //$this->getAnswerInput()->shouldMatch('/wawa/');
    }
    
    function it_renders_input_with_answer() {
        // @bug juiste laravel includes kunnen doen
        //$this->getAnswerInput("real answer")->shouldMatch('/real answer/1');
    }
}
