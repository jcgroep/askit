<?php

namespace spec\Jcgroep\Askit;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Jcgroep\Askit;

class QuestionMultiSpec extends ObjectBehavior
{
    function let()
    {
        $options = ['number' => 1, 
            'description' => 'description',
            'validators' => 'required',
            'postfix' => 'postfix',
            'size' => 10];
        $this->beConstructedWith('phpspec::1', 'question base', $options);
    }
    
    function it_is_initializable()
    {
        $this->shouldHaveType('Jcgroep\Askit\QuestionMulti');
    }
    
    function it_adds_options_in_constructor() {
        
        $options = ['options' => [1=> 1]];
        $this->beConstructedWith('phpspec::1', 'question base', $options);
        $this->getOptions()->shouldHaveCount(1);
    }
    
    function it_adds_1_option() {
        $this->resetOptions();
        $this->addOption(1,1);
        $this->getOptions()->shouldHaveCount(1);
    }
    
    function it_adds_multiple_options() {
        $this->resetOptions();
        $this->addOptions( [1 => 1, 2 => 2]);
        $this->getOptions()->shouldHaveCount(2);
    }
    
    function it_removes_an_option() {
        $this->resetOptions();
        $this->addOption('phpspec',1);
        $this->removeOption('phpspec');
        $this->getOptions()->shouldHaveCount(0);
    }
    
    function it_returns_false_when_removing_unfound_option() {
        $this->removeOption('phpspec')->shouldEqual(false);
    }
    
}
