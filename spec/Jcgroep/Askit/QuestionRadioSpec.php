<?php

namespace spec\Jcgroep\Askit;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Jcgroep\Askit;
use Jcgroep\Askit\QuestionRadio;

class QuestionRadioSpec extends ObjectBehavior
{
    function let()
    {
        $this->beConstructedWith('phpspec::1', 'question base');
    }
    
    function it_is_initializable()
    {
        $this->shouldHaveType('Jcgroep\Askit\QuestionRadio');
    }
    
    function it_stores_modus_attribute() {
        $this->setModus(QuestionRadio::MODUS_HORIZONTAL);
        $this->getModus()->shouldReturn(QuestionRadio::MODUS_HORIZONTAL);
    }
    
}
