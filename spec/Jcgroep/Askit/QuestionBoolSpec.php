<?php

namespace spec\Jcgroep\Askit;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Jcgroep\Askit;

class QuestionBoolSpec extends ObjectBehavior
{
    function let()
    {
        $this->beConstructedWith('phpspec::1', 'question base');
    }
    
    function it_is_initializable()
    {
        $this->shouldHaveType('Jcgroep\Askit\QuestionBool');
    }
    
    function it_contains_bool_values()
    {
        $this->getOptions()->shouldHaveCount(2);
    }
}
