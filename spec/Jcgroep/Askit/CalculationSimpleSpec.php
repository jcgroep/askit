<?php

namespace spec\Jcgroep\Askit;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Jcgroep\Askit;
use Exception;

class CalculationSimpleSpec extends ObjectBehavior {

    function let() {
        
    }

    function it_is_initializable() {
        $this->shouldHaveType('Jcgroep\Askit\CalculationSimple');
    }

    function it_can_calculate_a_simple_score() {
        $answers = ["identifier::1" => 2, "identifier::2" => 5];
        $this->getSimpleScore($answers)->shouldReturn(7);
    }

    function it_can_calculate_an_extended_score() {
        $answers = ["identifier::1" => 2, "identifier::2" => 5];
        $this->getExtendedScore($answers)->shouldHaveCount(1);
    }

}
