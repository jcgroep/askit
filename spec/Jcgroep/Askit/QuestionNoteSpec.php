<?php

namespace spec\Jcgroep\Askit;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Jcgroep\Askit;

class QuestionNoteSpec extends ObjectBehavior
{
    function let()
    {
        $this->beConstructedWith('phpspec::1', 'question base', ['title' => 'title', 'note' => 'note']);
    }
    
    function it_is_initializable()
    {
        $this->shouldHaveType('Jcgroep\Askit\QuestionNote');
    }
    
    function it_stores_title_attribute() {
        $this->getTitle()->shouldReturn("title");
    }

    function it_stores_note_attribute() {
        $this->getNote()->shouldReturn("note");
    }

}
