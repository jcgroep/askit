<?php

namespace spec\Jcgroep\Askit;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Jcgroep\Askit;
use Exception;

class AnalyzerCopdSpec extends ObjectBehavior {

    function let() {
        $this->beConstructedWith([500, 500, 550, 600]);
    }

    function it_is_initializable() {
        $this->shouldHaveType('Jcgroep\Askit\AnalyzerCopd');
    }

    function it_can_calculate_an_upward_trend() {
        $this->getTrend()->shouldReturn(1);
    }

    function it_can_calculate_a_downward_trend() {
        $this->beConstructedWith([600, 500]);
        $this->getTrend()->shouldReturn(-1);
    }

    function it_can_calculate_an_unchanged_trend() {
        $this->beConstructedWith([500, 500]);
        $this->getTrend()->shouldReturn(0);
    }

    function it_can_calculate_an_upward_text_score() {
        $this->getText()->shouldMatch("/Uw COPD klachten zijn verminderd sinds de start van uw behandeling/i");
    }

    function it_can_calculate_a_downward_text_score() {
        $this->beConstructedWith([500, 450]);
        $this->getText()->shouldMatch("/Uw COPD klachten zijn verergerd sinds de start van uw behandeling./i");
    }

    function it_can_calculate_a_stable_weekly_score() {
        $this->setScores([500, 500]);
        $this->getText()->shouldMatch("/De laatste week gaat het ongeveer gelijk aan de week hiervoor./i");
    }

    function it_can_calculate_a_really_positive_weekly_score() {
        $this->setScores([100, 500]);
        $this->getText()->shouldMatch("/De laatste week gaat het veel beter dan week hiervoor./i");
    }

    function it_can_calculate_a_really_negative_weekly_score() {
        $this->setScores([500, 100]);
        $this->getText()->shouldMatch("/minder dan de week hiervoor./i");
    }

}
