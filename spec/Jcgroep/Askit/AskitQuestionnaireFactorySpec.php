<?php

namespace spec\Jcgroep\Askit;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Jcgroep\Askit;
use Exception;

class AskitQuestionnaireFactorySpec extends ObjectBehavior {

    function let() {
    }

    function it_is_initializable() {
        $this->shouldHaveType('Jcgroep\Askit\AskitQuestionnaireFactory');
    }
    
    function it_can_build_a_hads_questionnaire() {
        $this->build("hads")->shouldReturnAnInstanceOf("Jcgroep\Askit\QuestionnaireWrapperBase");
    }
    
    function it_can_build_a_6minutewalking_questionnaire() {
        $this->build("6minutewalking")->shouldReturnAnInstanceOf("Jcgroep\Askit\QuestionnaireWrapperBase");
    }
    
    function it_can_build_a_Informedconsent_questionnaire() {
        $this->build("Informedconsent")->shouldReturnAnInstanceOf("Jcgroep\Askit\QuestionnaireWrapperBase");
    }
    
    function it_can_build_a_Dependency_questionnaire() {
        $this->build("Dependency")->shouldReturnAnInstanceOf("Jcgroep\Askit\QuestionnaireWrapperBase");
    }

    function it_can_build_a_ccq_questionnaire() {
        $this->build("Ccq")->shouldReturnAnInstanceOf("Jcgroep\Askit\QuestionnaireWrapperBase");
    }
    
    function it_cant_build_an_unfound_questionnaire() {
        $this->shouldThrow('Exception')->duringBuild("notfound");
    }
    
    function it_returns_available_questionnaires() {
        $this->getAvailableQuestionnaires()->shouldContain('CCQ');
    }

}
