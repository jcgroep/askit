# README #

Dit is de repository voor de Askit package. Askit is een package waarmee we binnen de JC Groep vragenlijst functionaliteit aan kunnen bieden binnen onze applicaties. Voor vragen over deze package kun je bij Matthijs terecht.

### Functionaliteit Askit ###
* Definiëren van vragenlijsten
* Afnemen van een vragenlijst
* Tonen van een read-only vragenlijst

### Wishlist functionaliteit ###
* Logica mogelijkheden voor tonen/verbergen van vragen
* Vraag voor vraag weergave inplaats van alle vragen in één keer

### Hoe te gebruiken ###

Definiëren van een vragenlijst

Maak een nieuwe class aan die extend van `QuestionCollection` 

    protected function createQuestions() {
        
        $q[] = $this->radio("CvaAfterExercise::1", "Zijn alle oefeningen gelukt?", ['validators' => 'required',"options" => [1 => 'Ja', 0 => 'Nee']]);
        $q[] = $this->textarea("CvaAfterExercise::2", "Wat lukte er niet?")
                ->dependsOn("CvaAfterExercise::1", 1);
        $q[] = $this->radio("CvaAfterExercise::3", "Heeft u nieuwe problemen ondervonden?", ['validators' => 'required',"options" => [1 => 'Ja', 0 => 'Nee']])
                ->dependsOn("CvaAfterExercise::2", 1);
        $q[] = $this->textarea("CvaAfterExercise::4", "Welke?");
        $q[] = $this->radio("CvaAfterExercise::5", "Denkt u dat u moeilijkere oefeningen aan kan?", ['validators' => 'required',"options" => [1 => 'Ja', 0 => 'Nee']]);
        
        return $q;
    }

Voeg een nieuwe regel toe aan de `QuestionnaireFactory`.`build` en `getAvailableQuestionnaires` functie zodat de vragenlijst binnen de applicatie ook aangeroepen kan worden. Om de vragenlijst ook echt toe te kunnen wijzen is het nodig om via de Vragenlijsten admin module ook een vragenlijst definitie aan te maken in de `questionnaires` tabel. Daarna weet de applicatie ook dat de vragenlijst er is en kan deze gebruikt worden op de diverse plaatsen. Via deze vragenlijsten kan de gebruikte naam, toelichting en inleiding ook bewerkt worden. 

### API QuestionCollection ###

De volgende functies kunnen gebruikt worden om een nieuwe vragenlijst te definiëren.

* `public function text($identifier, $label, array $options = null)`
* `public function multi($identifier, $label, array $options = null)`
* `public function bool($identifier, $label, array $options = null)`
* `public function radio($identifier, $label, array $options = null)`
* `public function scale($identifier, $label, array $options = null)`
* `public function note( $identifier, $title, $description = null)`
* `public function textarea($identifier, $label, array $options = null)`

### Beschikbare vragenlijsten ###
* 6 minuten looptest
* CCQ
* HADS