<?php
use Jcgroep\Askit\AskitPdfController;
use Jcgroep\Askit\AskitQuestionnaireDependencyController;
use Jcgroep\Askit\AskitQuestionnaireItemsController;
use Jcgroep\Askit\AskitQuestionnaireOptionsController;

Route::group(['middleware' => ['bindings', 'manage_questionnaires', 'web'], 'prefix' => '/askit', 'as' => 'askit.'], function ($router) {
    Route::post('/item/{questionnaire}/change-order', ['as' => 'item.changeOrder', 'uses' => AskitQuestionnaireItemsController::class . '@changeOrder']);
    Route::get('/item/{item}/move-up', ['as' => 'item.moveUp', 'uses' => AskitQuestionnaireItemsController::class . '@moveUp']);
    Route::get('/item/{item}/move-down', ['as' => 'item.moveDown', 'uses' => AskitQuestionnaireItemsController::class . '@moveDown']);
    Route::get('/item/update-table/{questionnaire}', ['as' => 'item.updateTable', 'uses' => AskitQuestionnaireItemsController::class . '@updateTable']);
    Route::get('/item/duplicate/{item}', ['as' => 'item.duplicate', 'uses' => AskitQuestionnaireItemsController::class . '@duplicate']);
    Route::resource('/item', AskitQuestionnaireItemsController::class);
    Route::get('/question-option/{option}/move-up', ['as' => 'questionOption.moveUp', 'uses' => AskitQuestionnaireOptionsController::class . '@moveUp']);
    Route::get('/question-option/{option}/move-down', ['as' => 'questionOption.moveDown', 'uses' => AskitQuestionnaireOptionsController::class . '@moveDown']);
    Route::resource('/question-option', AskitQuestionnaireOptionsController::class, ['except' => ['index']]);
    Route::resource('/dependency', AskitQuestionnaireDependencyController::class, ['except' => ['index']]);
    Route::get('/download/questionnaire/{questionnaire}', ['as' => 'questionnaires.downloadPdf', 'uses' => AskitPdfController::class . '@download']);
});
