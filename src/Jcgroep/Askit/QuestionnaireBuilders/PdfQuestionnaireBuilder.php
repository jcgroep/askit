<?php namespace Jcgroep\Askit;

use Jcgroep\BuildIt\FormElements\Text\TableElement;
use Jcgroep\Utils\Collections\BaseCollection;
use Lang;

class PdfQuestionnaireBuilder extends AbstractQuestionnaireBuilder
{

    public function render()
    {
        return $this->questionnaire->items->reduce(function ($html, QuestionnaireItem $item) {
            if(! $item->isQuestion()){
                return $html;
            }
            $html .= '<div style="page-break-inside: avoid;">';
            if($item->type == QuestionType::CHECKBOX){
                return $html . '[ &nbsp;] <strong>' . $item->title . '</strong><br /><br />';
            }
            if($item->type == QuestionType::HEADER){
                return $html . '<h3><strong>' . $item->title . '</strong></h3>';
            }
            $html .= '<strong>' . $item->title . '</strong><br />';
            if ($item->isQuestion() && $item->assistant->hasOptions()) {
                $html .= $this->renderOptions($item->assistant->options);
            } else {
                if ($item->type == QuestionType::BOOL) {
                    $html .= '<span style="font-style: italic;">' . trans('global.Yes') . ' / ' . trans('global.No') . '</span>';
                } elseif($item->type == QuestionType::TEXTAREA){
                    for($i = 0; $i < 4; $i++)
                        $html  .= '<br /><span style="color: gray">_________________________________________________________________</span><br />';
                } elseif($item->type == QuestionType::TABLE){
                    $html .= $this->renderTable($item);
                }
                elseif($item->type !== QuestionType::NOTE){
                    $html .= '<br /><span style="color: gray">______________________</span>';
                }


                $html .= ' ' . $item->assistant->postfix . '<br />';
            }
            return $html . '<br /></div>';
        }, '');
    }

    private function renderOptions(BaseCollection $options)
    {
        return $options->reduce(function ($html, QuestionOption $option) {
            return $html . $this->renderOption($option);
        }, '');
    }

    private function renderOption(QuestionOption $option)
    {
        $html = '<label>';
        if ($option->question->item->type == QuestionType::CHECKLIST) {
            $html .= '[ &nbsp;] ';
        } else {
            $html .= 'O ';
        }
        return $html . $option->label . '</label><br />';
    }

    private function renderTable($item)
    {
        $element = TableElement::create()
            ->withRows(QuestionItemLayout::getValue($item, 'layout_table_rows' ))
            ->withHeaders(QuestionItemLayout::getValue($item, 'layout_table_definition'))
            ->disable();


        return $element->renderElement();

    }
}