<?php namespace Jcgroep\Askit;

use Jcgroep\BuildIt\FormElements\Controls\CancelButton;
use Jcgroep\BuildIt\FormElements\Controls\ButtonGroup;
use Jcgroep\BuildIt\FormElements\Controls\SubmitButton;
use Jcgroep\BuildIt\FormElements\Groups\ButtonsElement;
use Lang;

abstract class AbstractQuestionnaireBuilder
{
    /**
     * @var ShowMode
     */
    protected $mode;
    /**
     * @var Questionnaire
     */
    protected $questionnaire;
    /**
     * @var Test
     */
    protected $test;
    protected $buttons;
    protected $questionItem;
    protected $autosave = true;

    public static function make(Questionnaire $questionnaire)
    {
        return new static($questionnaire);
    }

    public function __construct(Questionnaire $questionnaire)
    {
        $this->questionnaire = $questionnaire;
        $this->mode = new ShowMode(ShowMode::FILL_ITEM);
        $this->buttons = new ButtonGroup([new CancelButton(), new SubmitButton()]);
    }

    public function forTest(Test $test)
    {
        $this->test = $test;
        return $this;
    }

    public function inViewMode()
    {
        $this->mode = new ShowMode(ShowMode::SHOW_ANSWER);
        $this->withCloseButton();
        return $this;
    }

    public function withCloseButton()
    {
        $this->buttons = new ButtonGroup([CancelButton::make()->withTitle(trans('BuildIt::global.Close'))]);
        $this->autosave = false;
        return $this;
    }

    public function withoutButtons()
    {
        $this->buttons = new ButtonGroup();
        return $this;
    }

    public function showItem(QuestionnaireItem $questionItem)
    {
        if(!$questionItem->isLast()){
            $this->buttons = new ButtonGroup([new CancelButton(), SubmitButton::make()->withTitle(trans('BuildIt::global.Next'))]);
        }
        $this->questionItem = $questionItem;
        return $this;
    }

    protected function getItems(){
        if(isset($this->questionItem)){
            return [$this->questionItem];
        }
        return $this->questionnaire->items;
    }

    public abstract function render();

    public function __toString(): string
    {
        try{
            return $this->render();
        }catch (\Exception $e){
            var_dump($e->getMessage());
            dd($e->getTraceAsString());
        }
    }

    protected function getElement(QuestionnaireItem $item)
    {
        $formElement = QuestionnaireItemRendererFactory::build($this->mode, $item, $this->test->getAnswer($item))->render();
        //If you can't save the form, the form can't be submitted by a buttonsElement
        if ($formElement instanceof ButtonsElement && $this->buttons->canSubmit() == false) {
            $formElement->suppressSubmission();
        }
        return $formElement;
    }
}