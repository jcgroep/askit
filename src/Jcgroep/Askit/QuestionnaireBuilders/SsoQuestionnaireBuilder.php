<?php namespace Jcgroep\Askit;

use Input;
use Jcgroep\BuildIt\Form;
use Jcgroep\BuildIt\FormElements\Controls\ButtonGroup;
use Session;

class SsoQuestionnaireBuilder extends AbstractQuestionnaireBuilder
{

    public function render()
    {
        if ($this->test !== null && $this->test->isFinished()) {
            $this->inViewMode();
        }

        $form = Form::make([
            'sso.tests.update',
            'id' => isset($this->test)&& $this->test->id ? $this->test->id : -1,
            'hash' => Input::get('hash'),
            'current_question' => isset($this->questionItem) ? $this->questionItem->id : null
        ], 'PUT', Session::get('validationErrors', null));

        $form->withAutosave();

        if($this->test == null || $this->test->isFinished()){
            $form->withButtons(new ButtonGroup());
        }else{
            $form->withButtons($this->buttons);
        }

        foreach ($this->getItems() as $item) {
            $formElement = QuestionnaireItemRendererFactory::build($this->mode, $item, isset($this->test) ? $this->test->getAnswer($item): null);
            $form->addElement($formElement->render()); //Some magic here, render uses fill or show method, which return a FormElement
        }
        return $form;
    }

}