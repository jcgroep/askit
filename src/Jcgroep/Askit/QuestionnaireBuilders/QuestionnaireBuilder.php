<?php namespace Jcgroep\Askit;

use App\Html\Url\UrlHelper;
use Jcgroep\BuildIt\Form;
use Jcgroep\BuildIt\FormElements\Controls\DeleteButton;
use Jcgroep\BuildIt\FormElements\Controls\DropdownButtonGroup;
use Jcgroep\BuildIt\FormElements\Controls\LinkButton;
use Jcgroep\BuildIt\FormElements\NoteElement;
use Jcgroep\BuildIt\Modal;
use Lang;
use Request;
use Session;

class QuestionnaireBuilder extends AbstractQuestionnaireBuilder
{
    protected $actionButtons = false;
    protected $message;
    /**
     * @var DropdownButtonGroup
     */
    protected $buttonGroup;

    public function __construct(Questionnaire $questionnaire)
    {
        parent::__construct($questionnaire);
        $this->buttonGroup = DropdownButtonGroup::make();
    }

    public function withDeleteButton()
    {
        $this->buttonGroup->withButton(DeleteButton::make()
            ->withRoute('tests.destroy')
            ->withDeleteId($this->test->id)
        );
    }

    public function withActionButtons()
    {
        $this->buttonGroup->withButton(LinkButton::make()
            ->withLink(UrlHelper::make(Request::url())->addAttribute('force-edit', 1))
            ->withDataAttribute('ajax', 'data-ajax')
            ->withDataAttribute('target', '#show-task'));
        $this->buttonGroup->withButton(LinkButton::make()
            ->withLink(route('test.export', $this->test))
            ->withTitle(trans('actions.Download')));
        return $this;
    }

    public function withMessage($message)
    {
        $this->message = $message;
        return $this;
    }

    public function render()
    {
        if (!isset($this->test)) {
            $form = Form::make('tests.store', 'PUT', Session::get('validationErrors', null));
            $this->test = Test::create([
                'status' => Test::STATUS_NEW
            ]);
        } else {
            $form = Form::make(['tests.update', $this->test->id, 'current_question' => isset($this->questionItem) ? $this->questionItem->id : null], 'PUT', Session::get('validationErrors', null));
        }
        $form->withId('questionnaire-form');

        $modal = Modal::create($this->questionnaire->name, 'fill-questionnaire-modal', $form)
            ->inAjaxModal();
        if (!$this->buttonGroup->getButtons()->isEmpty()) {
            $modal->addHeaderButton($this->buttonGroup)
                ->withOption('closeButton', false);
        }

        $form->isAjax()
            ->withButtons($this->buttons)
            ->inModal($modal);

        if($this->autosave){
            $form->withAutosave();
        }

        if(isset($this->message)){
            $form->addElement(NoteElement::create()->withLabel($this->message)->withLabelClass(''));
        }

        foreach ($this->getItems() as $item) {
            $form->addElement($this->getElement($item));
        }

        return $form;
    }
}