<?php namespace Jcgroep\Askit;

use Input;
use Jcgroep\BuildIt\Form;
use Jcgroep\BuildIt\FormElements\Controls\ButtonGroup;
use Session;

class ApiQuestionnaireBuilder extends AbstractQuestionnaireBuilder
{

    public function render()
    {
        if ($this->test->isFinished()) {
            $this->inViewMode();
        }

        $form = Form::make(['api.v1.tasks.test.update', $this->test->id], 'PUT', Session::get('validationErrors', null))
            ->isAjax()
            ->withAutosave();

        if ($this->test->isFinished()) {
            $form->withButtons(new ButtonGroup());
        } else {
            $form->withButtons($this->buttons);
        }

        foreach ($this->getItems() as $item) {
            $formElement = QuestionnaireItemRendererFactory::build($this->mode, $item, $this->test->getAnswer($item));
            $form->addElement($formElement->render());
        }
        return $form;
    }
}