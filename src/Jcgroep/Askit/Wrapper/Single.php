<?php

namespace Jcgroep\Askit;
/**
 * @bug does not work at the moment
 */
class QuestionnaireWrapperSingle extends QuestionnaireWrapperBase {

    public function setCurrentPosition($identifier) {
        $this->currentPosition = $identifier;
    }
    
    public function getCurrentPosition() {
        return $this->currentPosition;
    }

     /**
     * Returns first unanswered question
     */
    public function getCurrentQuestion() {
        if ($this->currentPosition == null) {
            return $this->questions[0];
        } else {
            return $this->getQuestion($this->currentPosition);
        }
    }

    public function hasPreviousQuestion() {
        if ($this->getPreviousQuestion() === false) {
            return false;
        }
        return true;
    }

    public function getPreviousQuestion() {
        $current = $this->getCurrentQuestion();
        for ($i = 0; $i < count($this->questions); $i++) {
            if ($this->questions[$i]->getIdentifier() == $current->getIdentifier()) {
                if (isset($this->questions[$i - 1])) {
                    return $this->questions[$i - 1];
                }
            }
        }
        return false;
    }

    /**
     * Has this questionnaire a next question?
     * @return boolean
     */
    public function hasNextQuestion() {
        if ($this->getNextQuestion($this->getCurrentPosition() ) === false) {
            return false;
        }
        return true;
    }

    public function getNextQuestion($identifier) {
        $questionFound = false;
        foreach( $this->questions as $question) {
            if ($question->getIdentifier() == $identifier) {
                $questionFound = true;
            } else {
                if ($questionFound) {
                    if ($question->isVisible($this->getAnswers())) {
                        return $question;
                    }
                }
            }
        }
        return false;
    }
    
    public function addAnswers(array $answers) {
        unset($answers['_method']);
        unset($answers['_token']);
        unset($answers['form']);
        parent::addAnswers($answers);

        $identifier = ($answers['questionidentifier']);
        $this->setCurrentPosition($identifier);
        $question = $this->getQuestion($identifier);


        if ( $question->isValid($this->getAnswer($question) ) ) {
            $next = $this->getNextQuestion($question->getIdentifier() );
            if ( $next !== false) {
                $this->setCurrentPosition($next->getIdentifier() );
            }
        }
    }

    public function incrementCurrentPosition() {
        $question = $this->getCurrentQuestion();
        $next = $this->getNextQuestion($question->getIdentifier() );
        if ( $next !== false) {
             $this->setCurrentPosition($next->getIdentifier() );
        }
    }
    
    public function isComplete() {
        if ($this->isValid() && $this->hasNextQuestion() == false) {
            return true;
        }
        return false;
    }
}
