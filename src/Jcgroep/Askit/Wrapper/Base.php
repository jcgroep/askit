<?php

namespace Jcgroep\Askit;

class QuestionnaireWrapperBase
{
    protected $answers = [];
    protected $questionnaireRenderer;
    protected $questions;
    protected $test;
    protected $questionnaire;
    protected $readonly = false;

    public function __construct($questions)
    {
        $this->questions = $questions;

        $this->questionnaireRenderer = new QuestionnaireRenderer();

        $this->questionnaireRenderer->setView('body', 'Askit::questionnaires/_body')
            ->setView('header', 'Askit::questionnaires/all/_header')
            ->setView('footer', 'Askit::questionnaires/_footer')
            ->setView('question', 'Askit::questionnaires/_question');
    }

    public function render()
    {
        ob_start();
        echo $this->questionnaireRenderer->render($this);
        return ob_get_clean();
    }

    public function renderHeader()
    {
        echo $this->questionnaireRenderer->renderHeader($this);
    }

    public function renderFooter()
    {
        return $this->questionnaireRenderer->renderFooter($this);
    }

    public function setTest($test)
    {
        $this->test = $test;
        $answers = $test->answers;
        foreach ($answers as $answer) {
            $this->addAnswer($answer->question, $answer->answer);
        }

        $this->setQuestionnaire($test->questionnaire);
        if ($test->status == Test::STATUS_FINAL) {
            $this->setReadonly(true);
        }
    }

    public function getTest()
    {
        return $this->test;
    }

    public function setQuestionnaireRenderer(QuestionnaireRenderer $renderer)
    {
        $this->questionnaireRenderer = $renderer;
    }

    public function setQuestionnaire(Questionnaire $questionnaire)
    {
        $this->questionnaire = $questionnaire;
    }

    public function getQuestionnaire()
    {
        return $this->questionnaire;
    }

    public function addAnswers(array $answers)
    {
        $this->addAnswersMulti($answers);
    }

    public function addAnswersMulti(array $answers)
    {
        foreach ($answers as $identifier => $answer) {
            $this->addAnswer($identifier, $answer);
        }
    }

    public function addAnswer($identifier, $answer)
    {
        $question = $this->getQuestion($identifier);
        if ($question != null) {
            $this->answers[$identifier] = $answer;
        }
    }

    public function countQuestions()
    {
        return count($this->questions);
    }

    public function getQuestions()
    {
        return $this->questions;
    }

    public function getExcerptQuestions()
    {
        return $this->questions->excerpt();
    }

    public function getQuestion($identifier)
    {
        foreach ($this->questions as $question) {
            if ($question->getIdentifier() == $identifier) {
                return $question;
            }
        }
        return false;
    }

    public function hasAnswer($identifier)
    {
        if (is_object($identifier)) {
            $identifier = $identifier->getIdentifier();
        }

        if (array_key_exists($identifier, $this->answers)) {
            return true;
        }
        return false;
    }

    public function getAnswer($identifier, $default = null)
    {
        if (is_string($identifier)) {
            $question = $this->getQuestion($identifier);
        } else {
            $question = $identifier;
        }
        return $question->getAnswer($this->answers, $this->getQuestions(), $default);
    }

    /**
     * Returns all available answers
     * @return array
     */
    function getAnswers()
    {
        return $this->answers;
    }

    /**
     *
     * @param array $request
     * @return boolean
     */
    public function isPost($request)
    {
        if ($request['form'] == 'secret-form') {
            return true;
        }
        return false;
    }

    public function finalize()
    {
        $this->saveDraft();

        $this->test->status = \Test::STATUS_FINAL;
        $this->test->save();
    }

    public function saveDraft()
    {
        $questions = $this->getQuestions();
        foreach ($questions as $question) {
            if ($this->hasAnswer($question->getIdentifier())) {
                $answer = \Answer::firstOrCreate(['test_id' => $this->test->id, 'question' => $question->getIdentifier()]);
                $answer->answer = $this->getAnswer($question);
                $answer->save();
            }
        }
    }

    /**
     * @return boolean
     */
    public function isValid()
    {
        $validation = $this->getValidators();
        if ($validation->fails()) {
            return false;
        }
        return true;
    }

    public function getValidators()
    {
        $questions = $this->getQuestions();

        $values = [];
        $validations = [];
        foreach ($questions as $question) {
            if ($question->isVisible($this->getAnswers())) {
                $values[$question->getIdentifier()] = $this->getAnswer($question);
                $validations[$question->getIdentifier()] = $question->getValidators();
            }
        }
        return \Validator::make($values, $validations);
    }

    public function isComplete()
    {
        return $this->isValid();
    }

    public function countAnswered()
    {
        $questions = $this->getQuestions();
        $answered = 0;

        foreach ($questions as $question) {
            if ($this->getAnswer($question) != null) {
                $answered++;
            }
        }
        return $answered;
    }

    function isReadonly()
    {
        return $this->readonly;
    }

    function setReadonly($readonly)
    {
        $this->readonly = $readonly;
    }

    public function setView($type, $view)
    {
        $this->questionnaireRenderer->setView($type, $view);
    }


    /**
     * Single view
     */

    public function setCurrentPosition($identifier)
    {
        $this->currentPosition = $identifier;
    }

    public function getCurrentPosition()
    {
        return $this->currentPosition;
    }

    /**
     * Returns first unanswered question
     */
    public function getCurrentQuestion()
    {
        if ($this->currentPosition == null) {
            return $this->questions[0];
        } else {
            return $this->getQuestion($this->currentPosition);
        }
    }

    public function hasPreviousQuestion()
    {
        if ($this->getPreviousQuestion() === false) {
            return false;
        }
        return true;
    }

    public function getPreviousQuestion()
    {
        $current = $this->getCurrentQuestion();
        for ($i = 0; $i < count($this->questions); $i++) {
            if ($this->questions[$i]->getIdentifier() == $current->getIdentifier()) {
                if (isset($this->questions[$i - 1])) {
                    return $this->questions[$i - 1];
                }
            }
        }
        return false;
    }

    /**
     * Has this questionnaire a next question?
     * @return boolean
     */
    public function hasNextQuestion()
    {
        if ($this->getNextQuestion($this->getCurrentPosition()) === false) {
            return false;
        }
        return true;
    }

    public function getNextQuestion($identifier)
    {
        $questionFound = false;
        foreach ($this->questions as $question) {
            if ($question->getIdentifier() == $identifier) {
                $questionFound = true;
            } else {
                if ($questionFound) {
                    if ($question->isVisible($this->getAnswers())) {
                        return $question;
                    }
                }
            }
        }
        return false;
    }

    public function incrementCurrentPosition()
    {
        $question = $this->getCurrentQuestion();
        $next = $this->getNextQuestion($question->getIdentifier());
        if ($next !== false) {
            $this->setCurrentPosition($next->getIdentifier());
        }
    }
}
