<?php namespace Jcgroep\Askit;

use Illuminate\Support\ServiceProvider;

class AskitServiceProvider extends ServiceProvider {

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	/**
	 * Bootstrap the application events.
	 *
	 * @return void
	 */
	public function boot()
	{
        $this->loadViewsFrom(__DIR__.'/../../views/', 'Askit');
        $this->loadTranslationsFrom(__DIR__.'/../../lang/', 'Askit');
        if (! $this->app->routesAreCached()) {
            require __DIR__ . '/../../routes.php';
        }
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app->singleton("QuestionnaireFactory", function($app) {
            return new QuestionnaireFactory;
        });

        $this->app->booting(function() {
            $loader = \Illuminate\Foundation\AliasLoader::getInstance();
            $loader->alias('Askit', 'Jcgroep\Askit\Facade');
        });
	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return array();
	}

}
