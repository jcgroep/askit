<?php
namespace Jcgroep\Askit;

class AnalyzerCopd extends AnalyzerBase
{
    public function getTrend() {
        if ( $this->getLastScore() > $this->getFirstScore() ) {
            return self::UP;
        } else if ($this->getLastScore() < $this->getFirstScore() ) {
            return self::DOWN;
        }
        return self::LEVEL;
    }
    
    public function getText() {
        ob_start();
        if ( $this->getLastScore() >= $this->getFirstScore() ) {
            echo "Uw COPD klachten zijn verminderd sinds de start van uw behandeling. ";
        } else {
            echo "Uw COPD klachten zijn verergerd sinds de start van uw behandeling. ";
        }
        
        $var = $this->getLastScore() / $this->getShortTermAverage();
        switch( $var ) {
            case $var > 1.1:
                echo "De laatste week gaat het veel beter dan week hiervoor. ";
                break;
            case $var > 1:
                echo "De laatste week gaat het iets beter dan de week hiervoor. ";
                break;
            case $var == 1:
                echo "De laatste week gaat het ongeveer gelijk aan de week hiervoor. ";
                break;
            case $var < 0.9:
                echo "De laatste week ging het helaas minder dan de week hiervoor. ";
                break;
            case $var < 1:
                echo "De laatste week ging het helaas iets minder dan de week hiervoor. ";
                break;
        }
        return ob_get_clean();
    }
}