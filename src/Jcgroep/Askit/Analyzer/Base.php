<?php
namespace Jcgroep\Askit;

abstract class AnalyzerBase
{
    protected $scores = [];
    
    const UP = 1; 
    const LEVEL = 0; 
    const DOWN = -1; 
    public function __construct($scores) {
        $this->scores = $scores;
    }

    public function setScores($scores) {
        $this->scores = $scores;
    }
    public abstract function getTrend();
    
    public abstract function getText();
    
    protected function getAverage() {
        return array_sum($this->scores) / count($this->scores);
    }
    
    protected function getShortTermAverage() {
        if ( count($this->scores) > 2) {
            return ($this->scores[ count($this->scores) -1 ] + $this->scores[ count($this->scores) -2 ]) /2;
        } else {
            return $this->getAverage();
        }
    }
    
    protected function getFirstScore() {
        return $this->scores[0];
    }
    
    protected function getLastScore() {
        return $this->scores[ count($this->scores) -1 ];
    }
}

