<?php

namespace Jcgroep\Askit;

use Carbon;
use Patient;
use Illuminate\Support\Arr;
use App\Export\ExportService;
use App\SearchServices\TestsSearchService;

class QuestionnaireExport extends ExportService
{
    protected $questionnaire;
    protected $tests;
    protected $questions;

    public function forQuestionnaire(Questionnaire $questionnaire)
    {
        $this->questionnaire = $questionnaire;
        return $this;
    }

    public function forTests($tests)
    {
        $this->tests = $tests;
        return $this;
    }

    protected function getHeader()
    {
        $header = parent::getHeader();
        $header[] = trans('global.status');
        foreach ($this->getQuestions() as $question) {

            if ( is_object($question->title)) {
                $header[] = $question->title->__toString();
            } else {
                $header[] = $question->title;
            }
        }
        return $header;
    }

    protected function getTests()
    {
        $startDate = Arr::get($this->options, 'startPeriod', '') == '' ? null: Carbon::parse($this->options['startPeriod']);
        $endDate = Arr::get($this->options, 'endPeriod', '') == '' ? null: Carbon::parse($this->options['endPeriod']);

        $status = Arr::get($this->options, 'status', \Test::STATUS_FINAL) == \Test::STATUS_FINAL
            ? [\Test::STATUS_FINAL]
            : [\Test::STATUS_FINAL, \Test::STATUS_PROGRESS];

        $service = TestsSearchService::make()
            ->forQuestionnaire($this->questionnaire)
            ->forStatus($status );

        if ($startDate != null || $endDate != null) {
            $service->whereStartdateBetween($startDate, $endDate);
        }
        return $service->query()
            ->with('patient','task', 'answers')
            ->get();
    }

    public function toArray()
    {
        if(!isset($this->questionnaire)){
            throw new \InvalidArgumentException('questionnaire is required...');
        }
        $header = $this->getHeader();
        $data = [];
        foreach ($this->getTests() as $index => $test) {
            if($test->patient == null){
                continue;
            }
            $row = $this->getDefaultDataRow($test->patient, $test);

            if ($test->isFinished() ){
                $row[] = trans('global.finished');
            } else {
                $row[] = trans('global.started');
            }

            foreach ($this->getQuestions() as $question) {
                if(Arr::get($this->options, 'humanReadable', 'humanReadable') == 'humanReadable'){
                    $answer = $test->getCachedHumanReadableAnswer($question);;
                    if (!empty($answer) && $question->assistant->postfix) {
                        $answer .= " " . $question->assistant->postfix;
                    }
                }else{
                    $answer = $test->getCachedAnswer($question);;
                }

                $row[] = $answer;
            }
            $data[] = array_combine($header,$row);
        }
        if(empty($data)){
            return [$header];
        }

        return $data;
    }

    private function getQuestions()
    {
        //compact, only the items shown in the summary
        if(Arr::get($this->options, 'extended', 'extended') == 'excerptOnly'){
            return $this->questionnaire->items->onlyExcerpts();
        }
        //Extended, all question items except notes and headers
        return $this->questionnaire->items->filter(function(QuestionnaireItem $item){
            return !in_array($item->type,[QuestionType::NOTE, QuestionType::HEADER]);
        });
    }

    public function getDefaultDataRow(Patient $patient, $item)
    {
        if ($item->task != null && $item->task->start_date) {
            $row = [substr(md5($patient->id), 0, 6), $item->task->start_date->format('d-m-Y')];
        } else {
            $row = [substr(md5($patient->id), 0, 6), $item->created_at->format('d-m-Y')];
        }

        if (Arr::get($this->options, 'personalDetails', 'anonymous') == 'identified') {
            $row[] = $patient->patient_number;
            $row[] = $patient->fullName;
        }

        return $row;
    }
}

?>