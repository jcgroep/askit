<?php

namespace Jcgroep\Askit\Events;

use Illuminate\Queue\SerializesModels;
use Jcgroep\Askit\Test;

class TestWasStarted extends Event
{
    use SerializesModels;

    /**
     * @var Test
     */
    public $test;

    public function __construct(Test $test)
    {
        $this->test = $test;
    }
}