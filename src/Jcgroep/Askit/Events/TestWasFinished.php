<?php

namespace Jcgroep\Askit\Events;

use Illuminate\Queue\SerializesModels;
use Jcgroep\Askit\Test;

class TestWasFinished extends Event
{
    use SerializesModels;
    /**
     * @var Test
     */
    public $test;

    /**
     * Create a new event instance.
     *
     * @param Test $test
     */
    public function __construct(Test $test)
    {
        $this->test = $test;
    }
}
