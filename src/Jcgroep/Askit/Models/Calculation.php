<?php

use Jcgroep\Askit\CalculationStringBuilder;
use Jcgroep\Askit\QuestionnaireItem;
use Jcgroep\Utils\Collections\BaseCollection;
use Jcgroep\Askit\DependencyOperator;
use Illuminate\Support\Arr;

class Calculation extends \BaseModel
{

    protected $fillable = [
        'calculation_type',
        'custom_calculation_string',
        'operator',
        'reference_code',
    ];

    protected $appends = [
        'items'
    ];

    const REGEXP = "/[Item\d|sum|\d?|*|\/|+|\ |(|)]*/";

    public static function create(array $attributes = [])
    {
        $instance = parent::create($attributes);
        if (array_key_exists('question_selection', $attributes)) {
            $instance->items()->sync($attributes['question_selection']);
        }
        $instance->update($attributes);
        return $instance;
    }

    public function items()
    {
        return $this->belongsToMany(QuestionnaireItem::class);
    }

    public function options()
    {
        return $this->hasMany("Jcgroep\\Askit\\QuestionOption")->orderBy('order', 'asc');
    }

    public function item()
    {
        return $this->morphOne("Jcgroep\Askit\QuestionnaireItem", 'assistant');
    }

    public function update(array $attributes = [], array $options = [])
    {
        if (array_key_exists('question_selection', $attributes)) {
            $this->items()->sync($attributes['question_selection']);
        }
        $attributes['custom_calculation_string'] = CalculationStringBuilder::make()
            ->withHtml(Arr::get($attributes, 'custom_calculation_string', $this->custom_calculation_string))
            ->parse();
        parent::update($attributes, $options);
    }

    public function getOptionArray()
    {
        $options = [];
        foreach ($this->options as $option) {
            $options[$option->value] = $option->label;
        }
        return $options;
    }

    public function getCalculationString()
    {
        switch ($this->calculation_type) {
            case \Jcgroep\Askit\CalculationType::ALL:
                $items = $this->item->questionnaire->items
                    ->onlyUsefulForCalculations()
                    ->excludeIds([$this->item->id]);

                return \Jcgroep\Askit\CalculationStringBuilder::make()
                    ->forItems($items)
                    ->withOperator($this->operator)
                    ->get();
                break;
            case \Jcgroep\Askit\CalculationType::SELECTION:
                $items = $this->items
                    ->onlyUsefulForCalculations()
                    ->excludeIds([$this->item->id]);
                return \Jcgroep\Askit\CalculationStringBuilder::make()
                    ->forItems($items)
                    ->withOperator($this->operator)
                    ->get();

                break;
            case \Jcgroep\Askit\CalculationType::CUSTOM:
                return $this->custom_calculation_string;
        }
    }

    public function getHumanReadableCalculationString()
    {
        $calculation_string = $this->getCalculationString();
        return \Jcgroep\Askit\CalculationStringBuilder::make()->getHumanReadableVersion($calculation_string);
    }

    public function containsLoop($depth = 1)
    {
        if ($depth > 50) {
            return true;
        }

        $matches = $this->getBuildingBlocks();
        foreach ($matches as $match) {
            $id = str_replace("Item", "", $match);
            $item = \Jcgroep\Askit\QuestionnaireItem::find($id);

            if ($item->assistant_type == 'Calculation' && $item->assistant->containsLoop($depth + 1)) {
                return true;
            }
        }
        return false;
    }

    public function hasCorrectCalculationString()
    {
        if (count($this->getCorrectCalculationStringErrors()) == 0) {
            return true;
        }
        return false;
    }

    public function getCorrectCalculationStringErrors()
    {
        $errors = [];
        if ($this->containsLoop()) {
            $errors[] = trans("askit.calculations.containsLoop");
        }

        $calculationString = $this->getCalculationString();

        if (substr_count($calculationString, "(") != substr_count($calculationString, ")")) {
            $errors[] = trans("askit.calculations.bracketsDontMatch");
        }

        if (!preg_match(self::REGEXP, $calculationString)) {
            $errors[] = trans("askit.calculations.illegalCharacter") . " (" . $this->getIllegalCharactersInCalculationString() . ")";
        }
        return $errors;
    }

    private function getIllegalCharactersInCalculationString()
    {
        return preg_replace(self::REGEXP, "", $this->getCalculationString());
    }

    private function getBuildingBlocks()
    {
        preg_match_all("/ITEM_\d+/", $this->getCalculationString(), $matches);
        return $matches[0];
    }

    public function updateItemIds(array $conversions)
    {
        $calculationString = $this->custom_calculation_string;
        foreach ($this->getBuildingBlocks() as $buildingBlock) {
            $number = preg_replace('/[^0-9]+/', '', $buildingBlock);
            $calculationString = preg_replace("/ITEM\d+/", 'ITEM' . $conversions[$number]->id, $calculationString);
        }

        $this->update(['custom_calculation_string' => $calculationString]);
    }

    public function getAnswer(Jcgroep\Askit\Test $test)
    {
        $calculation_string = $this->getCalculationString();
        $calculator = new \Jcgroep\Askit\CalculatorBase();

        try {
            $answers = BaseCollection::make($this->getBuildingBlocks())->walk(function (&$value, &$key) use ($test) {

                $value = str_replace('ITEM_','', $value);
                $item = QuestionnaireItem::where('id', $value)->first();
                $key = "ITEM_" . $item->id;
                $value = $test->getAnswer($item);
            });

            $calculatedValue = $calculator->calc($calculation_string, $answers);
        } catch (Throwable $e) {
            \Log::info($e);
            return new Answer(['answer' => 'NaN']);
        }

        $postfix = "";
        if ($this->options->count() ) {
            $postfix = $this->calculatePostfix($calculatedValue) ;
        }
        return new Answer(['answer' => $calculatedValue.$postfix]);
    }

    protected function calculatePostfix($calculatedValue) {
        foreach( $this->options as $option) {
            $operator = new DependencyOperator($option->operator);
            if ($operator->compare($option->value, $calculatedValue) ) {
                return " (".$option->label.")";
            }
        }
    }
}
