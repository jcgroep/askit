<?php

namespace Jcgroep\Askit;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Collection;
use Illuminate\Support\Arr;

/**
 * Model containing the metatdate of q uestionnaire which can be used within the application.
 * Using this class enables you to use a custom name, description and
 * introduction for a specific questionnaire.
 * @property $items Collection
 * @property $official_name string
 * @property $name string
 */
class Questionnaire extends \BaseModel
{
    use SoftDeletes;

    protected $fillable = [
        'official_name',
        'name',
        'identifier',
        'version',
        'introduction',
        'duration_in_minutes',
        'sso',
        'questionmodus',
        'reference_code',
    ];

    protected $appends = [
        'items'
    ];

    public static function create(array $attributes = [])
    {
        $attributes['identifier'] = Arr::get($attributes, 'identifier', QuestionnaireBuilder::class);
        return parent::create($attributes);
    }

    /**
     * Every questionnare can have 0 or more tests attached to it
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function tests()
    {
        return $this->hasMany('Test');
    }

    public function items()
    {
        return $this->hasMany('\Jcgroep\Askit\QuestionnaireItem')->orderBy('order', 'asc');
    }

    public function getQuestions(array $skipQuestionIds = []): Collection
    {

        return $this->items()->where('assistant_type', Question::class)->whereNotIn('id', $skipQuestionIds)->get();
    }

    public function getCalculations(array $skipQuestionIds = []): Collection
    {

        return $this->items()->where('assistant_type', \Calculation::class)->whereNotIn('id', $skipQuestionIds)->get();
    }

    public function getValidationRules($input): array
    {
        $rules = [];
        foreach ($this->getQuestions() as $item) {
            $item->assistant->appendValidators($rules, $input);
        }
        return $rules;
    }

    public function getFirstQuestion(): QuestionnaireItem
    {
        return $this->getQuestions()->first();
    }

    public function getQuestion($itemId)
    {
        return $this->items()->where('id', $itemId)->first();
    }

    public function __toString()
    {
        return $this->name;
    }
}
