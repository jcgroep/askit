<?php namespace Jcgroep\Askit;

use DB;
use Illuminate\Database\Eloquent\SoftDeletes;
use Jcgroep\Askit\Validation\AnswerValidator;
use Jcgroep\Utils\Collections\BaseCollection;
use Lang;

/**
 * Answer model which is used to store answers belong to a test
 * @property Question questionObject
 * @property string answer
 * @property string humanReadableAnswer
 * @property string value
 */
class Answer extends \BaseModel
{

    use SoftDeletes;

    protected $fillable = [
        'test_id',
        'question',
        'answer',
    ];

    protected $casts = [
        'answer' => 'json'
    ];

    /**
     * Every answer belongs to exactly one test
     * @return Test
     */
    public function test()
    {
        return $this->belongsTo('Jcgroep\Askit\Test');
    }

    public function questionObject()
    {
        return $this->hasOne(Question::class, 'id', 'question');
    }

    /**
     * TODO: Moet efficienter kunnen...
     * @param array $attributes
     * @return Answer
     * @throws \App\FormValidation\FormValidationException
     * @throws \Exception
     */
    public static function create(array $attributes = [])
    {
        DB::beginTransaction();
        $answer = parent::create($attributes);
        if (!$answer->isValid($attributes)) {
            DB::rollBack();
        } else {
            DB::commit();
        }
        return $answer;
    }

    public function isValid(array $attributes)
    {
        $questionnaireItem = $this->questionObject->item;
        $validationInput = [$questionnaireItem->name => $attributes['answer']];
        $validator = new AnswerValidator($this, $this->test);
        $validator->withPrettyNames(['item_' . $questionnaireItem->id => $questionnaireItem->title]);

        if (!$validator->isValid($validationInput)) {
            $validator->validate($validationInput);
        }
        return true;
    }

    public function update(array $attributes = [], array $options = [])
    {
        (new AnswerValidator($this, $this->test))->validate([$this->questionObject->item->name => $attributes['answer']]);
        return parent::update($attributes, $options);
    }


    /**
     * Overwrite the parent function so the old answers which are not json encoded will still work.
     * @param string $value
     * @param bool $asObject
     * @return mixed|string
     */
    public function fromJson($value, $asObject = false)
    {
        $decoded = json_decode($value, ! $asObject);
        if (json_last_error() == JSON_ERROR_NONE) {
            return $decoded;
        }
        return $value;
    }

    public function getHumanReadableAnswerAttribute(): string
    {
        if ($this->questionObject == null) {
            return $this->answer ?? '';
        }

        if ($this->questionObject->item->type == QuestionType::BOOL) {
            return trans('Askit::questionnaires.bool.' . $this->answer);
        }

        if ($this->questionObject->hasOptions()) {
            if ($this->answer == null) {
                return '-';
            }

            $options = $this->questionObject->options()->whereIn('id', $this->extractAnswer($this->answer));
            $options = $this->getAnsweredOptions($this->answer);

            if (QuestionItemLayout::getValue($this->questionObject->item, "layout_show_option_value")) {
                return $this->returnOptionswithLabel($options);
            }

            return $options->count() == 0 ? $this->answer : BaseCollection::make($options->pluck('label'))->join(', ');
        }

        return $this->answer ?? '';
    }

    protected function returnOptionswithLabel($options)
    {
        return $options->count() == 0 ? $this->answer :
            BaseCollection::make($options->get()->map( function($option) { return $option -> label . " (". $option->value. ")";}))->join(', ');
    }

    protected function extractAnswer($answer) {
        if (is_array($answer) ) {
            return $answer;
        }

        if ($this->answerContainsArray($answer) ) {
            return explode("\",\"", substr($answer,2,-2));
        }

        if ($this->answerContainsJson($answer) ) {
            return array_keys(json_decode($answer, true));
        }
        return [$answer];
    }

    private function answerContainsArray($answer) {
        return strpos($answer, ']');
    }

    private function answerContainsJson($answer) {
        return strpos($answer, '}');
    }

    public function getValueAttribute()
    {
        if ($this->questionObject != null && $this->questionObject->hasOptions()) {
            $options = $this->getAnsweredOptions($this->answer);

            if ($options->count() > 0) {
                return BaseCollection::make($options->pluck('value'))->join(', ');
            }
        }
        return $this->answer;
    }

    protected function getAnsweredOptions($answer)
    {
        $options = $this->questionObject->options()->whereIn('id', $this->extractAnswer($this->answer));

        if ($options->count() == 0) {
            $options = $this->questionObject->options()->whereIn('value', $this->extractAnswer($this->answer));
        }

        return $options;
    }

    public function hasValue()
    {
        return isset($this->answer) && $this->answer != 'NaN';
    }

    public function __toString()
    {
        return $this->humanReadableAnswer;
    }
}
