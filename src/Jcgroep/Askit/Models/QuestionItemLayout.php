<?php namespace Jcgroep\Askit;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;
use Jcgroep\Utils\Collections\BaseCollection;

class QuestionItemLayout extends \BaseModel
{
    use SoftDeletes;

    const ORIENTATION = 'layout_orientation';
    const USE_OTHER = 'layout_use_other';
    const SLIDER_PREFIX = 'layout_scale_prefix';
    CONST SLIDER_POSTFIX = 'layout_scale_postfix';
    CONST SHOW_OPTION_VALUE = 'layout_show_option_value';

    protected $fillable = [
        'question_item_id',
        'key',
        'value',
        'reference_code',
    ];

    public static function has(QuestionnaireItem $item, $key)
    {
        return self::get($item, $key) !== null;
    }

    /**
     * @param QuestionnaireItem $item
     * @param $key
     * @return QuestionItemLayout|null
     */
    public static function get(QuestionnaireItem $item, $key)
    {
        return self::where('question_item_id', $item->id)->where('key', $key)->first();
    }

    public static function getValue(QuestionnaireItem $item, $key, $default = null)
    {
        if (self::has($item, $key)) {
            return self::get($item, $key)->value;
        }
        return $default;
    }

    public static function createOrUpdate(QuestionnaireItem $item, $key, $value)
    {
        if (self::has($item, $key)) {
            self::get($item, $key)->update(['value' => $value]);
        } else {
            self::create([
                'key' => $key,
                'value' => $value,
                'question_item_id' => $item->id
            ]);
        }
    }

    public static function updateAll(QuestionnaireItem $item, $attributes)
    {
        BaseCollection::make($attributes)->filter(function ($value, $key) {
            return Str::startsWith($key, 'layout_');
        })->each(function ($value, $key) use ($item) {
            QuestionItemLayout::createOrUpdate($item, $key, $value);
        });
    }
}