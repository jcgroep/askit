<?php namespace Jcgroep\Askit;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Collection;
use Jcgroep\Askit\Models\Traits\OrderableTrait;
use Calculation;

/**
 * @property Question question
 * @property string value
 * @property string label
 */
class QuestionOption extends \BaseModel
{
    use orderableTrait;
    use SoftDeletes;

    protected $fillable = [
        'question_id',
        'calculation_id',
        'order',
        'value',
        'label',
        'reference_code',
        'operator',
    ];

    public static function createWithRelations(array $attributes = [])
    {
        $model = Question::find($attributes['question_id']);
        if ($model == null) {
            $model = Calculation::find($attributes['calculation_id']);
            $attributes['question_id'] = "";
        }
        $attributes['order'] = $model->options->max('order') + 1;
        return self::create(collect($attributes)->only(['question_id',
            'calculation_id',
            'order',
            'value',
            'operator',
            'label'])->toArray());
    }

    public function question()
    {
        return $this->belongsTo(Question::class);
    }

    public function calculation()
    {
        return $this->belongsTo(\Calculation::class);
    }


    public function allOrderables(): Collection
    {
        if ($this->question_id) {
            return $this->question->options;
        }
        return $this->calculation->options;
    }

}
