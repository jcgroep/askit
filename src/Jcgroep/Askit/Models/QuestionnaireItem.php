<?php

namespace Jcgroep\Askit;

use Calculation;
use StringObject;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use App\Models\Utils\ValueObjects\HtmlObject;
use Illuminate\Database\Eloquent\SoftDeletes;
use Jcgroep\Utils\Collections\BaseCollection;
use Jcgroep\Askit\Models\Traits\OrderableTrait;

class QuestionnaireItem extends \BaseModel
{
    use OrderableTrait, SoftDeletes;

    protected $fillable = [
        'questionnaire_id',
        'assistant_type',
        'assistant_id',
        'name',
        'title',
        'type',
        'order',
        'show_in_excerpt',
        'layout_options',
        'reference_code',
    ];

    protected $appends = [
        'layoutOptions',
        'assistant',
        'dependencies'
    ];

    protected $casts = [
        'title' => StringObject::class
    ];

    public static function createWithRelations(array $attributes = [])
    {
        unset($attributes['_token']);
        $item = self::create(collect($attributes)->only(['questionnaire_id',
            'title',
            'type',
            'show_in_excerpt',
            'layout_options'])->toArray());

        $assistant = $item->createAssistantByType($attributes['type'], $attributes);
        $item->update([
            'name' => Arr::get($attributes, 'name', 'item_' . $item->id),
            'order' => $item->getNewOrder(),
            'assistant_type' => get_class($assistant),
            'assistant_id' => $assistant->id
        ]);
        QuestionItemLayout::updateAll($item, $attributes);

        return $item;
    }

    public function updateWithRelations(array $attributes = [], array $options = [])
    {
        if (($attributes['type'] == QuestionType::CALCULATION && $this->assistant_type != Calculation::class) ||
            ($attributes['type'] != QuestionType::CALCULATION && $this->assistant_type == Calculation::class)) {
            $assistant = $this->createAssistantByType($attributes['type'], $attributes);

            $attributes['assistant_type'] = get_class($assistant);
            $attributes['assistant_id'] = $assistant->id;

            $this->assistant->delete();
        }
        $updateSuccess = $this->update($attributes, $options);
        $this->assistant->update($attributes, $options);
        QuestionItemLayout::updateAll($this, $attributes);
        return $updateSuccess;
    }

    public function createAssistantByType($type, $attributes)
    {
        switch ($type) {
            case QuestionType::CALCULATION:
                return Calculation::create(collect($attributes)->only(['calculation_type',
                    'custom_calculation_string',
                    'question_selection',
                    'operator'])->toArray());
            default:
                return Question::create(collect($attributes)->only(['required',
                    'validators',
                    'postfix',
                    'validator_config1',
                    'validator_config2',])->toArray());
        }
    }

    /**
     * @param $name
     * @return QuestionnaireItem|null
     */
    public static function getByName($name)
    {
        return self::where('name', $name)->first();
    }

    public function layoutOptions()
    {
        return $this->hasMany(QuestionItemLayout::class, 'question_item_id');
    }

    /**
     * @return Questionnaire
     */
    public function questionnaire()
    {
        return $this->belongsTo(\Questionnaire::class)->withTrashed();
    }

    public function assistant()
    {
        return $this->morphTo();
    }

    public function dependencies()
    {
        return $this->hasMany(QuestionnaireItemDependency::class);
    }

    public function getDependencies()
    {
        $dependencies = [];
        foreach ($this->dependencies as $dependency) {
            $dependencies[] = [
                'element' => QuestionnaireItemRendererFactory::build(new ShowMode(ShowMode::FILL_ITEM), $dependency->dependend)->render(),
                'operator' => $dependency->dependend_operator,
                'value' => $dependency->dependend_value,
            ];
        }
        return $dependencies;
    }

    public function dependsOn(QuestionnaireItem $dependend, DependencyOperator $operator, $value)
    {
        QuestionnaireItemDependency::create([
            'questionnaire_item_id' => $this->id,
            'dependend_item_id' => $dependend->id,
            'dependend_operator' => $operator,
            'dependend_value' => $value,
        ]);
    }

    /**
     * This method works with either a Test object or the raw input
     * @param Test|array $test
     * @return bool
     */
    public function isVisible($test)
    {
        return !$this->isHidden($test);
    }

    /**
     * This method works with either a Test object or the raw input
     * @param Test|array $test
     * @return bool
     */
    public function isHidden($test)
    {
        foreach ($this->dependencies as $dependency) {
            if (!$dependency->met($test)) {
                return true;
            }
        }

        return false;
    }

    public function newCollection(array $models = [])
    {
        return new QuestionnaireItemCollection($models);
    }

    public function getRenderer()
    {
        return QuestionnaireItemRendererFactory::build(new ShowMode(ShowMode::FILL_ITEM), $this);
    }

    public function getHumanReadableAnswerAttribute($value)
    {
        return (new \Answer(['answer' => $value]))->humanReadableAnswer;
    }

    public function canBeUsedInCalculations()
    {
        switch ($this->type) {
            case QuestionType::BOOL:
            case QuestionType::SELECT:
            case QuestionType::RADIO:
            case QuestionType::SCALE:
            case QuestionType::STRING:
            case QuestionType::NUMBER:
            case QuestionType::CALCULATION:
            case QuestionType::CHECKLIST:

                return true;
        }
        return false;
    }

    public function allOrderables(): Collection
    {
        return $this->questionnaire->getQuestions();
    }

    public function copyDependencies(array $conversions)
    {
        foreach ($this->dependencies as $dependency) {
            $newQuestion = $conversions[$this->id];
            \Jcgroep\Askit\QuestionnaireItemDependency::create([
                'questionnaire_item_id' => $newQuestion->id,
                'dependend_item_id' => $conversions[$dependency->dependend_item_id]->id,
                'dependend_operator' => $dependency->dependend_operator,
                'dependend_value' => $dependency->dependend_value,
            ]);
        }
    }

    public function getCalculationIdentifierAttribute()
    {
        return "Item" . $this->id;
    }

    public function getShortTitleAttribute()
    {
        return $this->title->stripTags()->substr(0, 70, true);
    }

    public function setTitleAttribute($title)
    {
        if ($this->type == 'note') {
            $this->attributes['title'] = HtmlObject::make($title)->__toString();
        } else {
            $this->attributes['title'] = $title;
        }
    }

    public function delete()
    {
        QuestionnaireItemDependency::where('dependend_item_id', $this->id)->orWhere('questionnaire_item_id', $this->id)->delete();
        $this->assistant->delete();
        return parent::delete();
    }

    /**
     * Workaround for not copying the questionnaire. Somewhere the questionnaire is added to the array but I can't find where.
     * Here the questionnaire is just removed from the array.
     * @return array
     */
    public function toArray()
    {
        return BaseCollection::make(parent::toArray())->reject(function ($value, $key) {
            return $key == 'questionnaire';
        })->toArray();
    }

    public function isQuestion()
    {
        return $this->assistant_type == QuestionnaireItemType::QUESTION;
    }

    public function isMissingOptions()
    {
        if (!$this->isQuestion()) {
            return false;
        }

        if (!$this->assistant->hasOptions()) {
            return false;
        }

        return $this->assistant->options->isEmpty();
    }
}
