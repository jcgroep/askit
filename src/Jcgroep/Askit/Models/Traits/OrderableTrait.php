<?php namespace Jcgroep\Askit\Models\Traits;

use Illuminate\Support\Collection;

trait OrderableTrait
{
    public function setAfter($option)
    {
        $this->allOrderables()->filter(function ($orderable) use ($option) {
            return $orderable->order > $option->order;
        })->each(function ($orderable) {
            $orderable->update(['order' => $orderable->order + 1]);
        });
        $this->update(['order' => $option->order + 1]);
    }

    public function moveUp()
    {
        $option = $this->allOrderables()->filter(function ($orderable) {
            return $orderable->order < $this->order;
        })->last();

        if ($option == null) {
            return false;
        }
        $oldOrder = $this->order;
        $this->update(['order' => $option->order]);
        $option->update(['order' => $oldOrder]);
        return true;
    }

    public function moveDown()
    {
        $option = $this->allOrderables()->filter(function ($orderable) {
            return $orderable->order > $this->order;
        })->first();

        if ($option == null) {
            return false;
        }
        $oldOrder = $this->order;
        $this->update(['order' => $option->order]);
        $option->update(['order' => $oldOrder]);
        return true;
    }

    public function getNewOrder()
    {
        return $this->allOrderables()->max('order')+1;
    }

    public function isFirst()
    {
        return $this->order == $this->allOrderables()->min('order');
    }

    public function isLast()
    {
        return $this->order == $this->allOrderables()->max('order');
    }

    public abstract function allOrderables(): Collection;
}