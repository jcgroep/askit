<?php

namespace Jcgroep\Askit;

trait Testable
{
    public function getNumberOfTests() {
        return $this->tests->count();
    }

    public function getNumberOfOpenTests() {
        return $this->tests()
            ->where('status', "!=", 2)
            ->count();
    }
}