<?php

namespace Jcgroep\Askit;

use App\Models\Utils\Database;
use DB;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\QueryException;
use Jcgroep\Askit\Events\TestWasFinished;
use Jcgroep\Askit\Validation\TestValidator;
use Lang;

/**
 * Model class which stores filled in tests for a specific user
 * @property Questionnaire questionnaire
 * @property integer status
 */
class Test extends \BaseModel
{

    use SoftDeletes;

    const STATUS_NEW = 0;
    const STATUS_PROGRESS = 1;
    const STATUS_FINAL = 2;

    protected $fillable = [
        'patient_id',
        'questionnaire_id',
        'status',
        'targetroute'
    ];

    /**
     * Every test has 0 or more answers
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function answers()
    {
        return $this->hasMany(\Answer::class)->withTrashed();
    }

    public function getTitleAttribute()
    {
        return $this->questionnaire->name;
    }

    /**
     * Every test belongs to exactly 1 Questionnaire
     * @return Questionnaire
     */
    public function questionnaire()
    {
        return $this->belongsTo(Questionnaire::class)->withTrashed();
    }

    public function isFinished()
    {
        if ($this->status == self::STATUS_FINAL) {
            return true;
        }
        return false;
    }

    public function saveAnswers(array $input)
    {
        (new TestValidator($this->questionnaire, $input))->validate($input);
        foreach ($input as $questionName => $filledAnswer) {
            $this->saveAnswer($questionName, $filledAnswer);
        }
    }

    public function saveAnswer($questionName, $filledAnswer)
    {
        $item = $this->questionnaire->items()
            ->where('name', $questionName)
            ->first();

        if (is_array($filledAnswer) ) {
            $filledAnswer = json_encode($filledAnswer);
        }
        if($item == null) {
            return;
        }

        $answer = $this->answers()->where('question', $item->assistant->id)->first();
        if (isset($answer)) {
            return $answer->update(['answer' => $filledAnswer]);
        }

        return \Answer::create([
            'test_id' => $this->id,
            'question' => $item->assistant->id,
            'answer' => $filledAnswer
        ]);
    }

    public function getAnswerByName($name)
    {
        return $this->getAnswer(QuestionnaireItem::where('name', $name)->first());
    }

    public function getAnswer(QuestionnaireItem $item)
    {
        switch( $item->type) {
            case QuestionType::CALCULATION:
                return $item->assistant->getAnswer($this);
            default:
                $answer = $this->answers->where('question', $item->assistant->id)
                    ->sortByDesc('updated_at')
                    ->first();
                if($answer !== null){
                    return $answer;
                }
                return new Answer(['value' => 'NaN']);
        }
    }

    public function hasAnswer(QuestionnaireItem $item)
    {
        return $this->getAnswer($item) != null;
    }

    /*
     * TODO: can be done in a single query
     */
    public function updateStatus()
    {
        if($this->status == self::STATUS_FINAL){
            return;
        }
        $this->status = Test::STATUS_FINAL;
        foreach($this->questionnaire->getQuestions() as $question){
            if ($question->assistant->isRequired() && $question->isVisible($this) && !$this->hasAnswer($question)) {
                $this->status = Test::STATUS_PROGRESS;
            }
        }
        event(new TestWasFinished($this));
        $this->save();
    }
}
