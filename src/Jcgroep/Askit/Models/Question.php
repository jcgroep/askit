<?php namespace Jcgroep\Askit;

use Illuminate\Database\Eloquent\SoftDeletes;
use Jcgroep\Utils\Collections\BaseCollection;


/**
 * @property QuestionnaireItem item
 * @property BaseCollection options
 * @property string postfix
 */
class Question extends \BaseModel
{
    use SoftDeletes;

    protected $fillable = [
        'required',
        'validators',
        'postfix',
        'validator_config1',
        'validator_config2',
        'reference_code',
        'show_total',
    ];

    protected $appends = [
        'options'
    ];

    public static function create(array $attributes = [])
    {
        return parent::create(collect($attributes)->except(["options"])->toArray() );
    }

    public function update(array $attributes = [], array $options = [])
    {
        unset($this->options);
        return parent::update(collect($attributes)->except(["options"])->toArray(), $options);
    }

    public function item()
    {
        return $this->morphOne(QuestionnaireItem::class, 'Assistant');
    }

    public function addOption($value, $label)
    {
        return QuestionOption::createWithRelations([
            'question_id' => $this->id,
            'value' => $value,
            'label' => $label,
        ]);
    }

    public function getOptionArray()
    {
        $options = [];
        foreach ($this->options as $option) {
            $options[$option->value] = $option->label;
        }
        return $options;
    }

    public function hasOptions()
    {
        return $this->item->getRenderer() instanceof OptionsRenderer;
    }

    public function options()
    {
        return $this->hasMany("Jcgroep\\Askit\\QuestionOption")->orderBy('order', 'asc');
    }

    public function answers()
    {
        return $this->hasMany("Answer", "question");
    }

    public function getHumanReadableAnswerAttribute($value)
    {
        return $this->item->getHumanReadableAnswerAttribute($value);
    }

    public function getValueAttribute($value)
    {
        $option = $this->options()->where('id', $value)->first();
        if($option == null){
            return $value;
        }
        return $option->value;
    }

    /**
     * @param array|Test $input
     * @return string
     */
    public function getValidators($input = null)
    {
        return QuestionValidationType::getValidator($this, $input);
    }

    public function getPossibleValidators(): array {
        return $this->item->getRenderer()->getPossibleValidators();
    }

    public function hasPossibleValidators(): bool {
        return $this->item->getRenderer()->hasPossibleValidators();
    }

    /**
     * @param array $rules
     * @param array|Test $input
     */
    public function appendValidators(array &$rules, $input = null)
    {
        if (!empty($this->getValidators($input))) {
            $rules[str_replace(' ', '_', $this->item->name)] = $this->getValidators($input);
        }
    }

    // @todo fixen, waarom is dit nodig?
    public function getAttributesTest()
    {
        return [
            'postfix' => $this->postfix,
            'options' => $this->getOptionArray(),
            'validators' => $this->getValidators(),
            'calculation_identifier' => "Question" . $this->id
        ];
    }

    /**
     * @return QuestionnaireItem|null returns next questionnaire item, null if the given item is the last item
     */
    public function previous(Test $test = null)
    {
        $questions = $this->item->questionnaire->getQuestions();
        $item = $questions->pop();
        while ($item !== null && $item->id !== $this->item->id) {
            $item = $questions->pop();
        }
        $nextItem = $questions->pop();
        if ($nextItem != null && $nextItem->isHidden($test)) {
            $nextItem = $nextItem->assistant->previous($test);
        }

        return $nextItem;
    }

    /**
     * TODO: create own collection with efficient previous and next function, maybe something like (but holding the sort in mind): http://stackoverflow.com/questions/21909706/laravel-previous-and-next-records?lq=1
     * @return QuestionnaireItem|null returns next questionnaire item, null if the given item is the last item
     */
    public function next(Test $test)
    {
        $questions = $this->item->questionnaire->getQuestions();
        $item = $questions->shift();
        while ($item !== null && $item->id !== $this->item->id) {
            $item = $questions->shift();
        }
        $nextItem = $questions->shift();
        if ($nextItem != null && $nextItem->isHidden($test)) {
            $nextItem = $nextItem->assistant->next($test);
        }

        return $nextItem;
    }

    public function isRequired()
    {
        return $this->required;
    }
    public function delete()
    {
        $this->options->each->delete();
        $this->answers->each->delete();
        return parent::delete();
    }
}
