<?php

namespace Jcgroep\Askit;

class QuestionnaireItemType extends \Enum {

    const QUESTION = "Jcgroep\\Askit\\Question";
    const CALCULATION = "Jcgroep\\Askit\\Calculation";
}
