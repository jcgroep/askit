<?php namespace Jcgroep\Askit;

class RadioOrientation extends \Enum
{
    const VERTICAL = 'vertical';
    const HORIZONTAL = 'horizontal';
}