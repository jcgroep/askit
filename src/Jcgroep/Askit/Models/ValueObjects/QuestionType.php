<?php

namespace Jcgroep\Askit;

use Calculation;
use Lang;

class QuestionType extends \Enum
{

    const STRING = "string";
    const NUMBER = 'number';
    const DATE = 'date';
    const BOOL = "bool";
    const TEXTAREA = "textarea";
    const RADIO = "radio";
    const SELECT = "select";
    const MULTIBUTTON = "multiButton";
    const CHECKLIST = "checklist";
    const SCALE = "scale";
    const VAS = 'vas';
    const BORG = 'borg';
    const CHECKBOX = "checkbox";
    const CALCULATION = 'calculation';
    const HEADER = 'header';
    const NOTE = 'note';
    const TABLE = 'table';

    public function getClass()
    {
        if ($this->value == self::CALCULATION) {
            return Calculation::class;
        }
        return Question::class;
    }

    public static function grouped()
    {
        return [
            trans('Askit::questionnaireItems.text') => [
                self::STRING,
                self:: NUMBER,
                self::DATE,
                self::TEXTAREA,
                self::TABLE,
            ],
            trans('Askit::questionnaireItems.multi') => [
                self::BOOL,
                self::CHECKBOX,
                self::RADIO,
                self::SELECT,
                self::MULTIBUTTON,
                self::SCALE,
                self::VAS,
                self::BORG,
                self::CHECKLIST,
            ],
            trans('Askit::questionnaireItems.other') => [
                self::CALCULATION,
                self::HEADER,
                self::NOTE,
            ],
        ];
    }

    public static function onlyQuestions()
    {
        return [
            trans('Askit::questionnaireItems.text') => [
                self::STRING,
                self:: NUMBER,
                self::DATE,
                self::TEXTAREA,
                self::TABLE,
            ],
            trans('Askit::questionnaireItems.multi') => [
                self::BOOL,
                self::CHECKBOX,
                self::RADIO,
                self::SELECT,
                self::MULTIBUTTON,
                self::SCALE,
                self::VAS,
                self::BORG,
                self::CHECKLIST,
            ],
            trans('Askit::questionnaireItems.other') => [
                self::HEADER,
                self::NOTE,
            ],
        ];
    }

    public static function onlyCalculations()
    {
        return [
            trans('Askit::questionnaireItems.other') => [
                self::CALCULATION,
            ],
        ];
    }
}
