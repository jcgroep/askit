<?php
namespace Jcgroep\Askit;

class QuestionnaireStatusType
{
    const CONCEPT = 'Concept';
    const AVAILABLE = 'Available';
//    const ARCHIVE = 'Archive';

    private $status;

    public static function create($status)
    {
        return new self($status);
    }

    public function __construct($status)
    {
        $this->status = $status;
    }

    public static function getName($status)
    {
        switch($status){
            case QuestionnaireStatusType::CONCEPT:
                return ucfirst(\trans('global.concept'));
            case QuestionnaireStatusType::AVAILABLE:
                return ucfirst(\trans('global.final'));
//            case QuestionnaireStatusType::ARCHIVE:
//                return ucfirst(\trans('global.archive'));
            default:
                return ucfirst(\trans('global.unknown'));
        }
    }

    public static function all()
    {
        return [
            self::CONCEPT => self::getName(self::CONCEPT),
            self::AVAILABLE => self::getName(self::AVAILABLE),
//            self::ARCHIVE => self::getName(self::ARCHIVE)
        ];
    }

    public function __toString()
    {
        return self::getName($this->status);
    }
}
