<?php

namespace Jcgroep\Askit;

class CalculationType extends \Enum {

    const ALL = "all";
    const SELECTION = "selection";
    const CUSTOM = "custom";
}
