<?php

namespace Jcgroep\Askit;

class CalculationOperatorType extends \Enum {

    const SUM = "sum";
    const AVERAGE = "avg";
    const MAX = "max";
    const MIN = "min";
}
