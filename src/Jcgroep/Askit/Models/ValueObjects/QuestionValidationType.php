<?php

namespace Jcgroep\Askit;

use Carbon;
use Lang;

class QuestionValidationType extends \Enum
{
    const NONE = "none";
    const MIN = "min";
    const MAX = "max";
    const BETWEEN = "between";
    const DATE_IN_PAST = "date_in_past";
    const DATE_IN_FUTURE = "date_in_future";

    public static function getAdditionalFieldTranslations(string $type = null) {
        switch ($type) {
            case QuestionValidationType::MIN:
                return ['1' => trans('Askit::form.validation.min')];
            case QuestionValidationType::MAX:
                return ['2' => trans('Askit::form.validation.max')];
            case QuestionValidationType::BETWEEN:
                return ['1' => trans('Askit::form.validation.min'), '2' => trans('Askit::form.validation.max')];
            default:
                return [];
        }
    }

    /**
     * @param Question $question
     * @param array|Test $input
     * @return string
     */
    public static function getValidator( Question $question, $input = null) {
        if ( $question->item == null) {
            return "";
        }
        $validators = "";
        if ($question->required) {
            if(!isset($input) || $question->item->isVisible($input)){
                $validators .= "required|";
            }
        } else {
            $validators .= "nullable|";
        }

        if ( $question->item->type == QuestionType::NUMBER) {
            $validators .= "Numeric|";
        }

        if ( $question->item->type == QuestionType::DATE) {
            $validators .= "date|";
        }

        switch( $question->validators) {
            case self::MIN:
                return $validators . "min:" . $question->validator_config1;
            case self::MAX:
                return $validators . "max:" . $question->validator_config2;
            case self::BETWEEN:
                return $validators . "between:" . $question->validator_config1 . "," . $question->validator_config2;
            case self::DATE_IN_FUTURE:
                return $validators . "after:" . Carbon::now()->toDateString();
            case self::DATE_IN_PAST:
                return $validators . "before:" . Carbon::now()->toDateString();
        }
        return $validators;
    }
}
