<?php

namespace Jcgroep\Askit;

use Illuminate\Database\Eloquent\Collection;

class QuestionnaireItemCollection extends Collection
{

    public function suitableForDependency()
    {
        return $this->filter(function (QuestionnaireItem $item) {
            switch ($item->type) {
                case QuestionType::BOOL:
                case QuestionType::SELECT:
                case QuestionType::RADIO:
                case QuestionType::SCALE:
                case QuestionType::STRING:
                case QuestionType::NUMBER:
                case QuestionType::CALCULATION:
                case QuestionType::MULTIBUTTON:
                case QuestionType::CHECKBOX:
                    return true;
            }
            return false;
        });
    }

    public function onlyExcerpts()
    {
        return $this->filter(function ($item) {
            return $item->show_in_excerpt;
        })->reject(function (QuestionnaireItem $item) {
            switch ($item->type) {
                case QuestionType::NOTE:
                case QuestionType::HEADER:
                    return true;
            }
            return false;
        });
    }

    public function onlyUsefulForCalculations()
    {
        return $this->filter(function ($item) {
            return $item->canBeUsedInCalculations();
        });
    }

    public function excludeIds(Array $ids)
    {
        return $this->filter(function ($item) use ($ids) {
            return !in_array($item->id, $ids);
        });
    }
}
