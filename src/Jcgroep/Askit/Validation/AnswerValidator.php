<?php namespace Jcgroep\Askit\Validation;

use App\FormValidation\FormValidator;
use Jcgroep\Askit\Answer;
use Jcgroep\Askit\Test;

class AnswerValidator extends FormValidator
{
    protected $model = 'test';
    protected $validationRules = [];

    /**
     * @param Answer $answer
     * @param Test|array $input
     */
    public function __construct(Answer $answer, $input)
    {
        $answer->questionObject->appendValidators($this->validationRules, $input);
    }

    protected function getValidationRules($uniqueColumnIds = []){
        return $this->validationRules;
    }
}