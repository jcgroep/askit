<?php namespace Jcgroep\Askit\Validation;

use App\FormValidation\FormValidator;
use Jcgroep\Askit\Questionnaire;
use Lang;
use Validator;
use Session;

class TestValidator extends FormValidator
{
    protected $model = 'test';
    protected $validationRules;
    protected $fields;

    public function __construct(Questionnaire $questionnaire, array $input)
    {
        $this->validationRules = $questionnaire->getValidationRules($input);
        $questions = $questionnaire->getQuestions();

        $prettyNames = [];
        foreach( $questions as $question) {
            $prettyNames["item_" . $question->id] = '';
        }

        $this->withPrettyNames($prettyNames);
    }

    protected function getValidationRules($uniqueColumnIds = []){
        return $this->validationRules;
    }

    public function withValidationRules($validationRules){
        
    }
}