<?php namespace Jcgroep\Askit\Validation;

use App\FormValidation\FormValidator;
use Jcgroep\Askit\QuestionnaireItemDependency;
use Lang;

class DependencyValidator extends FormValidator
{
    protected $model = 'dependency';
    protected $dependency;

    public function __construct(QuestionnaireItemDependency $dependency)
    {
        $this->dependency = $dependency;
    }

    protected function isAllowed(array $formData)
    {
        if($this->dependency->containsLoop()){
            return ['dependend_item_id' => trans('Askit::questionnaires.infiniteDependencyLoop')];
        }

        return [];
    }
}