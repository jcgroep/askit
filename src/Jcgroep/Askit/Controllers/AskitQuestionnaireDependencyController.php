<?php

namespace Jcgroep\Askit;


use App\FormValidation\QuestionnaireDependencyFormValidator;
use App\Http\Controllers\Controller;
use Input;
use Jcgroep\Askit\Forms\QuestionDependencyForm;
use Lang;

class AskitQuestionnaireDependencyController extends Controller
{
    public function create()
    {
        $item = QuestionnaireItem::findOrFail(Input::get('item_id'));
        $dependency = new QuestionnaireItemDependency([
            'questionnaire_item_id' => $item->id,
            'dependend_operator' => Input::get('dependend_operator', DependencyOperator::EQUAL_TO)
        ]);

        if (Input::has('dependend_item_id')) {
            $dependency->dependend = QuestionnaireItem::findOrFail(Input::get('dependend_item_id'));
        } else {
            $dependency->dependend = $item->questionnaire->getQuestions()->first();
        }

        $form = QuestionDependencyForm::make(null, $dependency)
            ->withData(['questionnaire' => $item->questionnaire])
            ->renderCreateForm();

        if (Input::has('dependend_item_id')) {
            return $form->getContentOnly();
        }
        return $form;
    }

    public function store()
    {
        $input = Input::all();

        QuestionnaireDependencyFormValidator::validateAllInput();
        $dependency = QuestionnaireItemDependency::create(
            [
                'questionnaire_item_id' => $input['questionnaire_item_id'],
                'dependend_item_id' => $input['dependend_item_id'],
                'dependend_operator' => $input['dependend_operator'][0],
                'dependend_value' => $input['dependend_value'],
            ]
        );

        return [
            'message' => trans('Askit::questionnaireDependencies.createSuccess'),
            'replaceSelector' => '#question-details',
            'replaceInner' => true,
            'replaceHtml' => (new AskitQuestionnaireItemsController())->show($dependency->item)->render(),
            'closeModal' => true,
        ];
    }

    public function edit(QuestionnaireItemDependency $dependency)
    {
        if (Input::has('dependend_item_id')) {
            $dependency->dependend = QuestionnaireItem::findOrFail(Input::get('dependend_item_id'));
        }

        $form = QuestionDependencyForm::make(null, $dependency)
            ->withData(['questionnaire' => $dependency->item->questionnaire])
            ->renderEditForm();

        if (Input::has('dependend_item_id')) {
            return $form->getContentOnly();
        }
        return $form;
    }

    public function update(QuestionnaireItemDependency $dependency)
    {
        $input = Input::all();
        $dependency->update(
            [
                'questionnaire_item_id' => $input['questionnaire_item_id'],
                'dependend_item_id' => $input['dependend_item_id'],
                'dependend_operator' => $input['dependend_operator'][0],
                'dependend_value' => $input['dependend_value'],
            ]
        );

        return [
            'message' => trans('Askit::questionnaireDependencies.updateSuccess'),
            'replaceSelector' => '#question-details',
            'replaceInner' => true,
            'replaceHtml' => (new AskitQuestionnaireItemsController())->show($dependency->item)->render(),
            'closeModal' => true,
        ];
    }

    public function destroy(QuestionnaireItemDependency $dependency)
    {
        $dependency->delete();

        return [
            'message' => trans('Askit::questionnaireDependencies.deleteSuccess'),
            'replaceSelector' => '#question-details',
            'replaceInner' => true,
            'replaceHtml' => (new AskitQuestionnaireItemsController())->show($dependency->item)->render(),
        ];
    }


}