<?php

namespace Jcgroep\Askit;

use Illuminate\Contracts\Validation\UnauthorizedException;
use Input;
use Jcgroep\Askit\Events\TestWasStarted;

/**
 * Controller which handles the showing and answering of a specific Test
 */
class AskitSsoController extends AskitController
{

    public function show($id)
    {
        $test = \Test::findOrFail($id);
        /** @var $test Test */

        if (Input::get('hash') != $test->hash) {
            abort(403);
        }

        event(new TestWasStarted($test));

        $questionnaireRenderer = SsoQuestionnaireBuilder::make($test->questionnaire)
            ->forTest($test);

        return view('Askit::tests/sso/show', compact('test', 'questionnaireRenderer'));
    }

    public function update($id)
    {
        $test = \Test::findOrFail($id);
        abort_unless(Input::get('hash') == $test->hash, 403);

        $javascriptParameters = parent::update($id);
        $test = $test->fresh();
        event(new TestWasStarted($test));
        if (Input::get('autosave', 'false') == 'false') {
            if (array_key_exists('closeModal', $javascriptParameters)) {
                $questionnaireRenderer = SsoQuestionnaireBuilder::make($test->questionnaire)
                    ->forTest($test);
                return view('Askit::tests/sso/show', compact('test', 'questionnaireRenderer'));
            }
            if (array_key_exists('replaceHtml', $javascriptParameters)) {
                return $javascriptParameters['replaceHtml'];
            }
        }

        return $javascriptParameters;
    }
}
