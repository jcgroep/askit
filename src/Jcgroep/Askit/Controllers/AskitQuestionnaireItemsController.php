<?php namespace Jcgroep\Askit;

use Illuminate\Routing\Controller;
use Input;
use Jcgroep\Utils\Collections\BaseCollection;
use Lang;

class AskitQuestionnaireItemsController extends Controller
{

    public function index()
    {
        $questionnaire = Questionnaire::findOrFail(Input::get('questionnaire_id'));
        return view('Askit::questionnaires.edit.allQuestions', ['questionnaire' => $questionnaire, 'items' => $questionnaire->items]);
    }

    public function create()
    {
        $item = new QuestionnaireItem(Input::all());
        $assistant = (new QuestionType(Input::get('type', 'string')))->getClass();
        $item->assistant = new $assistant(Input::all());
        $form = QuestionnaireItemRendererFactory::build(new ShowMode(ShowMode::CREATE_ITEM), $item)
            ->render();

        if (Input::has('_token')) {
            return $form->getContentOnly();
        }

        return $form;
    }

    public function duplicate(QuestionnaireItem $item)
    {
        $newItem = $item->copy();
        $newItem->name = "item_" . $newItem->id;
        $newItem->update();
        $newItem->setAfter($item);
        return redirect(route('questionnaires.show', [$item->questionnaire->id, 'selectedQuestionItemId' => $newItem->id]));
    }

    public function store()
    {
        $input = Input::all();

        if (!isset($input['required']) ) {
            $input['required'] = false;
        }
        $item = QuestionnaireItem::createWithRelations($input);
        $item->questionnaire->triggerAnswerCache();

        return [
            'message' => trans('Askit::questionnaireItems.createSuccess'),
            'closeModal' => true,
            'replaceSelector' => '#question-details',
            'replaceInner' => true,
            'replaceHtml' => $this->show($item)->render()
        ];
    }

    public function show(QuestionnaireItem $item)
    {
        return QuestionnaireItemRendererFactory::build(new ShowMode(ShowMode::VIEW_ITEM), $item);
    }

    public function edit(QuestionnaireItem $item)
    {
        foreach (Input::all() as $inputParam => $value) {
            $item[$inputParam] = $value;
            $item->assistant[$inputParam] = $value;
        }

        $form = QuestionnaireItemRendererFactory::build(new ShowMode(ShowMode::EDIT_ITEM), $item)->render();
        if (Input::has('_token')) {
            return $form->getContentOnly();
        }
        return $form;
    }

    public function update(QuestionnaireItem $item)
    {
        Input::merge(['validator_config1' => Input::get('validator_config1', ''), 'validator_config2' => Input::get('validator_config2', '')]);

        $input = Input::all();
        $item->updateWithRelations($input);
        $item->questionnaire->triggerAnswerCache();
        return [
            'message' => trans('Askit::questionnaireItems.updateSuccess'),
            'closeModal' => true,
            'replaceSelector' => '#question-details',
            'replaceInner' => true,
            'replaceHtml' => $this->show($item->fresh())->render()
        ];
    }

    public function destroy(QuestionnaireItem $item)
    {
        Input::merge(['questionnaire_id' => $item->questionnaire_id]);
        $item->delete();

        return [
            'message' => trans('Askit::questionnaireItems.deleteSuccess'),
            'closeModal' => true,
            'replaceSelector' => '#questionnaire-details',
            'replaceInner' => true,
            'replaceHtml' => $this->index()->render()
        ];
    }

    public function updateTable(Questionnaire $questionnaire)
    {
        $items = $questionnaire->items()->get(); // not using $questionnaire->items because that does not refresh the items collection
        return view('Askit::questionnaires.edit.questionnaireItemsTable', compact('items', 'questionnaire'));
    }

    public function moveUp(QuestionnaireItem $item)
    {
        $item->moveUp();
        return $this->updateTable($item->questionnaire);
    }

    public function moveDown(QuestionnaireItem $item)
    {
        $item->moveDown();
        return $this->updateTable($item->questionnaire);
    }

    public function changeOrder(Questionnaire $questionnaire)
    {
        BaseCollection::make(Input::get('order'))->each(function ($id, $order) use ($questionnaire) {
            $item = QuestionnaireItem::where('questionnaire_id', $questionnaire->id)->findOrFail($id);
            $item->update(['order' => $order]);
        });
    }
}
