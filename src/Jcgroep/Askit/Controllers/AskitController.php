<?php

namespace Jcgroep\Askit;

use App\Http\Controllers\Controller as BaseController;
use Input;
use Jcgroep\Askit\Events\TestWasFinished;
use Jcgroep\Askit\Events\TestWasStarted;
use Lang;
use PDF;
use Redirect;
use Session;

/**
 * Controller which handles the showing and answering of a specific Test
 */
class AskitController extends BaseController
{

    public function show($id)
    {
        $test = \Test::findOrFail($id);
        /** @var $test Test */
        $builderName = $test->questionnaire->identifier;
        $questionnaire = $builderName::make($test->questionnaire)
            ->forTest($test);

        event(new TestWasStarted($test));
        return $questionnaire;
    }

    public function update($id)
    {
        $test = \Test::findOrFail($id);
        if (Input::get('autosave', 'false') !== 'false') {
            return $this->autosave($test);
        }

        return $this->handleDefaultModeQuestionnaire($test);
    }

    public function download($id)
    {
        $questionnaire = \Questionnaire::findOrFail($id);
        $tests = $questionnaire->tests;
        $export = new QuestionnaireExport();
        $export->export($questionnaire, $tests);
    }

    public function downloadPdf(\Test $test)
    {
        $questions = $test->questionnaire->items->filter(function(QuestionnaireItem $item) use($test){
            return $item->isVisible($test);
        });
        $pdf = PDF::loadView('Askit::pdf.test', compact('test', 'questions'));
        return $pdf->download($test->questionnaire->name . '-' . $test->created_at->format('d-m-Y') . '.pdf');
    }

    private function handleSingleModeQuestionnaire(\Test $test)
    {

        $question = QuestionnaireItem::findOrFail(Input::get('current_question'));

        $test->saveAnswer($question->name, Input::get($question->name));

        $nextQuestion = $question->assistant->next($test);

        if ($nextQuestion !== null) {
            Input::merge(array('current_question' => $nextQuestion->id));
            return [
                'replaceInner' => true,
                'replaceSelector' => '#show-task',
                'replaceHtml' => $this->show($test->id)->render()->render()
            ];
        }
        $test->updateStatus();
        return [
            'message' => trans('Askit::questionnaires.savedFinal'),
            'closeModal' => true
        ];
    }

    private function handleDefaultModeQuestionnaire(\Test $test)
    {
        $test->saveAnswers(Input::all());
        $test->updateStatus();

        if ($test->isFinished()) {
            return [
                'message' => trans('Askit::questionnaires.savedFinal'),
                'closeModal' => true
            ];

        }
        return [
            'message' => trans('Askit::questionnaires.savedDraft')
        ];
    }

    private function autosave(\Test $test)
    {
        if(empty(Input::get('saved_item', ''))){
            return;
        }
        $question = QuestionnaireItem::where('name', str_replace('[]','', trim(Input::get('saved_item'))))->firstOrFail();
        $test->saveAnswer($question->name, Input::get($question->name));
    }

}
