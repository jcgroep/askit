<?php namespace Jcgroep\Askit;

use Illuminate\Routing\Controller;

class AskitPdfController extends Controller
{
    public function download(Questionnaire $questionnaire)
    {
        $questionnaireRenderer = PdfQuestionnaireBuilder::make($questionnaire);

        //return view('Askit::pdf/questionnaire', compact('questionnaireRenderer', 'questionnaire'));
        return \PDF::loadView('Askit::pdf/questionnaire', compact('questionnaireRenderer', 'questionnaire'))->download($questionnaire->name . '.pdf');
    }
}