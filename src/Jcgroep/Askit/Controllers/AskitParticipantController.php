<?php

namespace Jcgroep\Askit;

use App\SearchServices\TestsSearchService;
use Illuminate\Routing\Controller as BaseController;

/**
 * Class ParticipantController
 * @package Jcgroep\Askit
 */
abstract class AskitParticipantController extends BaseController
{

    public function index()
    {
        $participants = $this->getAllParticipants();



        return \View::make('Askit::participants/index', compact('participants'));
    }

    public function show($id)
    {
        $participant = $this->findParticipant($id);
        $tests = $participant->tests;

        return \View::make('Askit::participants/show', compact('participant', 'tests'));
    }

    public function assignQuestionnaire($id)
    {

    }

    protected abstract function getAllParticipants();

    protected abstract function findParticipant($id);
}
