<?php

namespace Jcgroep\Askit;

use Jcgroep\Utils\Collections\BaseCollection;
use Jcgroep\Utils\ValueObjects\StringObject;

class CalculatorBase
{
    function calc($expression, $answers) {

        $compiler = \Hoa\Compiler\Llk::load(
            new \Hoa\File\Read('hoa://Library/Math/Arithmetic.pp')
        );

        $visitor = $this->getPreparedVisitor($answers);
        $ast = $compiler->parse($expression);

        return $visitor->visit($ast);
    }

    /**
     * @param BaseCollection $conversion a collection with as key the variable name and as value the actual value to use
     * @return \Hoa\Math\Visitor\Arithmetic
     */
    protected function getPreparedVisitor(BaseCollection $conversion) {
        $visitor = new \Hoa\Math\Visitor\Arithmetic();

        $visitor = $this->addFunctions($visitor);

        $conversion->each(function($answer, $identifier) use($visitor){
            if ( $visitor->getConstants()->offsetExists(strtoupper($identifier)) ) {
                $visitor->getConstants()->offsetUnset(strtoupper($identifier));
            }

            $value = $answer == null ? 0 : $answer->value;
            if ($value instanceof StringObject) {
                $value = $value->value;
            }
            $visitor->addConstant(strtoupper($identifier), $value);
        });
        return $visitor;
    }

    protected function addFunctions($visitor) {
        $visitor->addFunction('rand', function ($min, $max) {
            return mt_rand($min, $max);
        });

        $visitor->addFunction('if', function($condition, $than, $else) {
            return $condition ? $than : $else;
        });

        $visitor->addFunction('lt', function($left, $right) {
            return $left < $right;
        });

        $visitor->addFunction('gt', function($left, $right) {
            return $left > $right;
        });

        $visitor->addFunction('eq', function($left, $right) {
            return $left == $right;
        });

        $visitor->addFunction('switch', function($firstCondition, $firstValue) {
            $arguments = collect(func_get_args());

            $default = null;
            if ( $arguments->count() %2 == 1) {
                $default = $arguments->pop();
            }
            for( $i = 0; $i < $arguments->count(); $i++) {
                if ( $arguments[$i] ) {
                    return $arguments[$i +1];
                }
                $i++;
            }
            return $default;
        });

        return $visitor;
    }
}