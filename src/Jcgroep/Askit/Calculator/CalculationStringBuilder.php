<?php

namespace Jcgroep\Askit;

use Illuminate\Support\Str;

class CalculationStringBuilder
{
    protected $items;
    protected $operator;
    protected $html;

    static function make()
    {
        return new self;
    }

    public function forItems(QuestionnaireItemCollection $items): CalculationStringBuilder
    {
        $this->items = $items;
        return $this;
    }

    public function withOperator($operator): CalculationStringBuilder
    {
        $this->operator = $operator;
        return $this;
    }

    public function get() : string
    {
        $ids = $this->items->map(function ($item) {
            return strtoupper("Item_" . $item->id);
        });
        return $this->operator . "(" . implode(",", $ids->toArray()) . ")";
    }

    public function withHtml($html)
    {
        $this->html = $html;
        return $this;
    }

    public function parse()
    {
        $html = $this->html;
        $html = preg_replace_callback('/<span[^\>]*data-item-id=\"([0-9]*)\"[^\<]*<\/span>/s', function ($matches) {
            return 'ITEM_' . $matches[1];
        }, $html);
        $string = '';
        $index = 0;
        while ($index < strlen($html)) {
            $char = $html[$index];
            if (in_array($char, ['I', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '+', '-', '/', '*', 'i', 's', 'a', 'm', 'c', 'f', 'g', 'l','r', 'e', '(', ')', ','])) {
                $remaining = substr($html, $index);
                switch ($char) {
                    case 'I':
                        if (Str::startsWith($remaining, 'ITEM_')) {
                            preg_match('/^(ITEM_\d+)/', $remaining, $matches);
                            $string .= $matches[0];
                            $index += strlen($matches[0])-1;
                        }
                        break;
                    case '/':
                        if (preg_match('/^\/[^\<]*\>/', $remaining) === 0) { //if it is not an html tag
                            $string .= $char;
                        }
                        break;
                    default:
                        if(in_array($char, ['s', 'a', 'm', 'c', 'f','g','l','r','e', 'i'])){
                            if (preg_match('/^(sum|avg|min|max|ceil|floor|gt|lt|rand|eq|switch|if)/', $remaining, $matches) !== 0) {
                                $string .= $matches[0];
                                $index += strlen($matches[0])-1;
                            }
                            break;
                        }
                        $string .= $char;
                        break;
                }
            }
            $index++;
        }
        return $string;
    }

    public function getHumanReadableVersion($calculation_string = '') : string
    {
        $calculation_string = preg_replace_callback("/ITEM_\d+/", function ($matches) {
            $id = str_replace("ITEM_", "", $matches[0]);
            $item = \Jcgroep\Askit\QuestionnaireItem::find($id);
            return "<span data-item-id=\"$item->id\" class='item' contenteditable=\"false\">$item->title</span>";
        }
            , $calculation_string);

        $calculation_string = preg_replace_callback("/sum|avg/", function ($matches) {
            return "<span class='function'>" . $matches[0] . "</span>";
        }
            , $calculation_string);

        $calculation_string = str_replace(",",", ", $calculation_string);
        $calculation_string = str_replace("(","( ", $calculation_string);
        $calculation_string = str_replace(")",") ", $calculation_string);
        return $calculation_string;
    }
}