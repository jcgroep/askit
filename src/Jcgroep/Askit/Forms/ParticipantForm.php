<?php namespace Jcgroep\Askit\Forms;

use Jcgroep\BuildIt\AbstractForm;
use Jcgroep\BuildIt\Form;
use Jcgroep\BuildIt\FormElements\Text\EmailElement;
use Jcgroep\BuildIt\FormElements\Text\TextAreaElement;
use Jcgroep\BuildIt\FormElements\Text\TextElement;
use Jcgroep\BuildIt\Modal;
use Lang;

class ParticipantForm extends AbstractForm
{
    public function renderEditForm()
    {
        $form = Form::make(['participants.update', $this->target->id], 'PUT', $this->errors);
        $modal = Modal::create(['cancel', 'save'], trans("user.EditPatient"), 'patient-edit', $form)->addAttribute('data-reset-on-close');

        return $this->renderForm($form, $modal);
    }

    public function renderCreateForm()
    {
        $form = Form::make('participants.store', 'POST', $this->errors);
        $modal = Modal::create(['cancel', 'save'], trans("user.NewPatient"), 'participant-create', $form)->addAttribute('data-reset-on-close');

        return $this->renderForm($form, $modal);
    }

    private function renderForm(Form $form, Modal $modal)
        {
            $firstName = TextElement::create()
                ->withName('first_name')
                ->withLabel(trans("user.FirstName"))
                ->withDefaultValue($this->target->user !== null ? $this->target->user->first_name : null);

            $surnamePrefixes = TextElement::create()
                ->withLabel(trans("user.SurnamePrefixes"))
                ->withName('surname_prefixes')
                ->withDefaultValue($this->target->user !== null ? $this->target->user->surname_prefixes : null);

            $lastName = TextElement::create()
                ->withLabel(trans('user.LastName'))
                ->withName('last_name')
                ->isRequired()
                ->withDefaultValue($this->target->user !== null ? $this->target->user->last_name : null);

            $email = EmailElement::create()
                ->withLabel(trans('user.Email'))
                ->withName('email')
                ->withDefaultValue($this->target->user !== null ? $this->target->user->email : null);

            $patientNumber = TextElement::create()
                ->withLabel(trans('user.PatientNumber'))
                ->withName('patient_number')
                ->withDefaultValue($this->target->patient_number);

            $comments = TextAreaElement::create()
                ->withLabel(trans("global.Comments"))
                ->withName('comments')
                ->withDefaultValue($this->target->user !== null ? $this->target->user->comments : null);

            return $form->inModal($modal)
                ->addElement($firstName)
                ->addElement($surnamePrefixes)
                ->addElement($lastName)
                ->addElement($email)
                ->addElement($patientNumber)
                ->addElement($comments)
                ->withErrors($this->errors)
                ->render();
        }
}