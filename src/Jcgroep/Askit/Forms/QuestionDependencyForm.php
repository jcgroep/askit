<?php namespace Jcgroep\Askit\Forms;

use Jcgroep\Askit\DependencyOperator;
use Jcgroep\Askit\QuestionnaireItemRendererFactory;
use Jcgroep\Askit\ShowMode;
use Jcgroep\BuildIt\AbstractForm;
use Jcgroep\BuildIt\Form;
use Jcgroep\BuildIt\FormElements\DefaultDropdownElement;
use Jcgroep\BuildIt\FormElements\Groups\ButtonsElement;
use Jcgroep\BuildIt\FormElements\Groups\SelectElement;
use Jcgroep\BuildIt\FormElements\Text\HiddenInput;
use Jcgroep\BuildIt\Modal;
use Lang;

class QuestionDependencyForm extends AbstractForm
{

    public function renderEditForm()
    {
        $form = Form::make(['askit.dependency.update', $this->target->id], 'PUT', $this->errors);
        $modal = Modal::create(trans("Askit::questionnaires.editDependency"), 'edit-dependency', $form)->addAttribute('data-reset-on-close');

        return $this->renderForm($form, $modal);
    }

    public function renderCreateForm()
    {
        $form = Form::make(['askit.dependency.store'], 'POST', $this->errors);
        $modal = Modal::create(trans("Askit::questionnaires.createDependency"), 'create-dependency', $form)->addAttribute('data-reset-on-close');

        return $this->renderForm($form, $modal);
    }

    private function renderForm(Form $form, Modal $modal)
    {
        $form->isAjax();
        $question = SelectElement::create()
            ->withName('dependend_item_id')
            ->isRequired()
            ->withLabel(trans('Askit::questionnaires.Question'))
            ->withDataAttribute('update-ajax', 'update-ajax')
            ->withDataAttribute('target', '#' . $form->getId())
            ->withDataAttribute('url', $this->getUpdateUrl())
            ->withCollection($this->data['questionnaire']->getQuestions([$this->target->questionnaire_item_id])->suitableForDependency(), 'title')
            ->withDefaultValue($this->target->dependend_item_id);

        $dependency = HiddenInput::create()
            ->withName('questionnaire_item_id')
            ->withDefaultValue($this->target->questionnaire_item_id);

        $operator = SelectElement::create()
            ->withName('dependend_operator')
            ->withLabel(trans_choice('Askit::questionnaires.dependencies', 1))
            ->withArray(DependencyOperator::all())
            ->withDefaultValue($this->target->getOperator());

        $value = QuestionnaireItemRendererFactory::build(new ShowMode(ShowMode::FILL_ITEM), $this->target->dependend)->render()
            ->withName('dependend_value')
            ->withLabel(trans('Askit::questionnaires.value'))
            ->withLabelClass('col-md-3 control-label')
            ->withClasses(['col-md-4'])
            ->withDefaultValue($this->target->dependend_value);

        if ($value instanceof ButtonsElement) {
            $value->suppressSubmission();
        }

        return $form->inModal($modal)
            ->addElement($question)
            ->addElement($dependency)
            ->addElement($operator)
            ->addElement($value)
            ->withErrors($this->errors);
    }

    private function getUpdateUrl()
    {
        if ($this->target->id == null) {
            return route('askit.dependency.create', ['item_id' => $this->target->questionnaire_item_id]);
        }
        return route('askit.dependency.edit', [$this->target->id]);
    }
}