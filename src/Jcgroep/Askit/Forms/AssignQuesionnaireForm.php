<?php namespace Jcgroep\Askit\Forms;

use Jcgroep\BuildIt\AbstractForm;
use Jcgroep\BuildIt\Form;
use Jcgroep\BuildIt\FormElements\DefaultDropdownElement;
use Jcgroep\BuildIt\Modal;
use Lang;

class AssignQuestionnaireForm extends AbstractForm
{

    public function renderEditForm()
    {
        return $this->renderCreateForm();
    }

    public function renderCreateForm()
    {
        $form = Form::make(['participants.assignQuestionnaire', $this->target->id], 'POST', $this->errors);
        $modal = Modal::create(['cancel', 'save'], trans("questionnaires.assignQuestionnaire"), 'assign-questionnaire', $form)->addAttribute('data-reset-on-close');

        return $this->renderForm($form, $modal);
    }

    private function renderForm(Form $form, Modal $modal)
    {
        $questionnaireDropdown = DefaultDropdownElement::create()
            ->withName('questionnaire_id')
            ->withLabel(trans_choice('questionnaires.Questionnaires', 1))
            ->withCollection(\Questionnaire::all());

        return $form->inModal($modal)
            ->addElement($questionnaireDropdown)
            ->withErrors($this->errors)
            ->render();
    }
}