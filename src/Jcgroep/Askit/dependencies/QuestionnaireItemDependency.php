<?php

namespace Jcgroep\Askit;

use DB;
use Illuminate\Database\Eloquent\SoftDeletes;
use Jcgroep\Askit\Validation\DependencyValidator;

class QuestionnaireItemDependency extends \BaseModel
{
    use SoftDeletes;

    protected $fillable = [
        'questionnaire_item_id',
        'dependend_item_id',
        'dependend_operator',
        'dependend_value',
    ];

    protected $appends = [
        'dependend'
    ];

    public static function create(array $attributes = [])
    {
        DB::beginTransaction();
        $instance = parent::create($attributes);
        $validator = new DependencyValidator($instance);
        if (!$validator->isValid($attributes)) {
            DB::rollBack();
            $validator->validate($attributes);
        }
        DB::commit();
        return $instance;
    }

    public function dependend()
    {
        return $this->belongsTo(QuestionnaireItem::class, 'dependend_item_id');
    }

    public function item()
    {
        return $this->belongsTo(QuestionnaireItem::class, 'questionnaire_item_id');
    }

    public function getOperator()
    {
        return new DependencyOperator($this->dependend_operator);

    }

    /**
     * @param Test|array $test
     * @return bool
     */
    public function met($test)
    {
        if ($test instanceof Test) {
            $answerObject = $test->getAnswer($this->dependend);
            if ($answerObject == null) {
                return false;
            }
            $answer = $answerObject->value;
        } else {
            $answer = isset($test[$this->dependend->name]) ? $test[$this->dependend->name] : null;
        }
        return $this->getOperator()->compare($answer, $this->dependend_value);
    }

    public function containsLoop($depth = 1)
    {
        if ($depth > 50) {
            return true;
        }

        if ($this->dependend != null) {
            $dependencies = $this->dependend->dependencies;
            foreach ($dependencies as $dependency) {
                if ($dependency->containsLoop($depth + 1)) {
                    return true;
                }
            }
        }
        return false;
    }
}
