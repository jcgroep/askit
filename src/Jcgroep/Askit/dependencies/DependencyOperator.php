<?php namespace Jcgroep\Askit;

use Lang;

class DependencyOperator extends \Enum
{
    const EQUAL_TO = '=';
    const NOT_EQUAL_TO = '!=';
    const LARGER_THAN = '>';
    const SMALLER_THAN = '<';
    const LARGER_THAN_OR_EQUAL = '>=';
    const SMALLER_THAN_OR_EQUAL = '<=';

    public static function all()
    {
        return trans('Askit::questionnaireDependencies.operators');
    }

    public function compare($value, $compareTo)
    {
        switch ($this->value) {
            case self::EQUAL_TO:
                return $value == $compareTo;
            case self::LARGER_THAN:
                return $value > $compareTo;
            case self::SMALLER_THAN:
                return $value < $compareTo;
            case self::LARGER_THAN_OR_EQUAL:
                return $value >= $compareTo;
            case self::SMALLER_THAN_OR_EQUAL:
                return $value <= $compareTo;
            case self::NOT_EQUAL_TO:
                return $value != $compareTo;
            default:
                return $value == $compareTo;
        }
    }

    public function getCompareValue(): string
    {
        if($this->value == self::EQUAL_TO){
            return '==';
        }
        return $this->value;
    }
}