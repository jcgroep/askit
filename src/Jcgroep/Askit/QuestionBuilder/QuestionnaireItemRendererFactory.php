<?php

namespace Jcgroep\Askit;

class QuestionnaireItemRendererFactory
{
        static function build(ShowMode $mode, QuestionnaireItem $item, Answer $answer = null): BaseRenderer {
            if($item->type != null){
                $objectName = 'Jcgroep\\Askit\\' . ucfirst($item->type) . 'Renderer';
                return new $objectName($mode, $item, $answer);
            }
            return new StringRenderer($mode, $item, $answer);
        }
}