<?php namespace Jcgroep\Askit;

use Jcgroep\BuildIt\FormElements\FormElement;
use Jcgroep\BuildIt\FormElements\Groups\RadioElement;
use Lang;

class BoolRenderer extends QuestionRenderer
{
    protected function createItem()
    {
        return QuestionForm::make(null, $this->item)
            ->withoutPostfix()
            ->renderCreateForm();
    }

    protected function editItem()
    {
        return QuestionForm::make(null, $this->item)
            ->withoutPostfix()
            ->renderEditForm();
    }

    public function fill(): FormElement
    {
        return $this->getElement(RadioElement::class)
            ->withSimpleOptions(['1', '0'], 'Askit::questionnaires.bool');
    }
}