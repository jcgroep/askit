<?php namespace Jcgroep\Askit;

use Jcgroep\BuildIt\FormElements\FormElement;
use Jcgroep\BuildIt\FormElements\Groups\CheckboxGroupElement;

class ChecklistRenderer extends OptionsRenderer
{

    public function fill(): FormElement
    {
        $answer = null;
        if($this->answer !== null){
            //for some unknown reason $this->answer->answer does not work, between the __get and getAttributeValue the value becomes null...
            $answer = $this->answer->getAttributeValue('answer');
        }

        $element = $this->getElement(CheckboxGroupElement::class)
            ->withCollection($this->item->assistant->options, 'label')
            ->withDefaultValue($answer);

        if(QuestionItemLayout::getValue($this->item, QuestionItemLayout::USE_OTHER, false) == true){
            $element->withOther();
        }
        return $element;
    }

    protected function createItem()
    {
        return QuestionChecklistForm::make(null, $this->item)->renderCreateForm();
    }

    protected function editItem()
    {
        return QuestionChecklistForm::make(null, $this->item)->renderEditForm();
    }
}