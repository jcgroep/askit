<?php namespace Jcgroep\Askit;

use Jcgroep\BuildIt\FormElements\FormElement;
use Jcgroep\BuildIt\FormElements\Text\TextAreaElement;

class TextareaRenderer extends QuestionRenderer
{
    public function fill(): FormElement{
        return $this->getElement(TextAreaElement::class)->withPlaceholder(trans('Askit::questionnaireItems.defaultPlaceholder'));
    }
}