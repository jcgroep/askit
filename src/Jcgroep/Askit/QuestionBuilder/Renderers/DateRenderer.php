<?php namespace Jcgroep\Askit;

use Jcgroep\BuildIt\FormElements\FormElement;
use Jcgroep\BuildIt\FormElements\Text\DateElement;

class DateRenderer extends StringRenderer
{
    public function fill(): FormElement
    {
        return $this->getElement(DateElement::class);
    }

    public function getPossibleValidators(): array {
        return [QuestionValidationType::NONE,
            QuestionValidationType::DATE_IN_FUTURE,
            QuestionValidationType::DATE_IN_PAST];
    }
}