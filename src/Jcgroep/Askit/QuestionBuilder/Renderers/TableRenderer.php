<?php namespace Jcgroep\Askit;

use Jcgroep\BuildIt\FormElements\FormElement;
use Jcgroep\BuildIt\FormElements\Text\TableElement;

class TableRenderer extends QuestionRenderer
{
    protected function createItem()
    {
        return TableForm::make(null, $this->item)
            ->renderCreateForm();
    }

    protected function editItem()
    {
        return TableForm::make(null, $this->item)
            ->renderEditForm();
    }

    public function fill(): FormElement
    {
        return $this->getElement(TableElement::class)
            ->withRows(QuestionItemLayout::getValue($this->item, 'layout_table_rows' ))
            ->withHeaders(QuestionItemLayout::getValue($this->item, 'layout_table_definition'));
    }
}