<?php

namespace Jcgroep\Askit;

use Jcgroep\BuildIt\FormElements\FormElement;
use Jcgroep\BuildIt\FormElements\DefaultDropdownElement;
use App\Models\taskables\BorgScore;
use Lang;

class BorgRenderer extends StringRenderer
{
    public function fill(): FormElement
    {
        return $this->getElement(DefaultDropdownElement::class)
            ->withArray(BorgScore::all());
    }
}