<?php

namespace Jcgroep\Askit;

use Jcgroep\BuildIt\FormElements\FormElement;
use Jcgroep\BuildIt\FormElements\Text\ScaleElement;

class VasRenderer extends StringRenderer
{
    protected function createItem()
    {
        return VasForm::make(null, $this->item)->renderCreateForm();
    }

    protected function editItem()
    {
        return VasForm::make(null, $this->item)->renderEditForm();
    }

    public function fill(): FormElement
    {
        return $this->getElement(ScaleElement::class)
            ->withOptions(range(0,100))
            ->withScalePrefix(QuestionItemLayout::getValue($this->item, QuestionItemLayout::SLIDER_PREFIX))
            ->withScalePostfix(QuestionItemLayout::getValue($this->item, QuestionItemLayout::SLIDER_POSTFIX))
            ->hideValues();
    }
}