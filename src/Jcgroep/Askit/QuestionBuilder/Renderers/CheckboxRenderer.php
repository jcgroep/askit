<?php namespace Jcgroep\Askit;

use Jcgroep\BuildIt\FormElements\CheckboxElement;
use Jcgroep\BuildIt\FormElements\FormElement;

class CheckboxRenderer extends QuestionRenderer
{
    public function fill(): FormElement
    {
        $element = CheckboxElement::create()
            ->withLabel($this->item->title)
            ->withName($this->item->name)
            ->inputFirst()
            ->withLabelClass('question control-label pull-left small-margin-right')
            ->withClasses(['xs-top-margin', 'answer', 'pull-left'])
            ->withDefaultValue(isset($this->answer) ? $this->answer->answer : null);

        $element->dependsOnMany($this->item->getDependencies());

        if ($this->item->assistant->isRequired()) {
            $element->isRequired();
        }
        return $element;
    }
}