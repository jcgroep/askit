<?php namespace Jcgroep\Askit;

use Jcgroep\BuildIt\FormElements\FormElement;
use Jcgroep\BuildIt\FormElements\Text\PlainTextElement;
use Jcgroep\BuildIt\FormElements\Text\NullElement;

class CalculationRenderer extends BaseRenderer
{

    protected function createItem()
    {
        return CalculationForm::make(null, $this->item)->renderCreateForm();
    }

    protected function editItem()
    {
        return CalculationForm::make(null, $this->item)->renderEditForm();
    }

    public function fill(): FormElement
    {
        return NullElement::create();
    }

    public function show(): FormElement
    {
        return PlainTextElement::create()
            ->withLabel($this->item->title)
            ->withLabelClass('col-md-12 question control-label')
            ->withClasses(['col-md-12 answer'])
            ->withDefaultValue(isset($this->answer) ? $this->answer->answer : null)
            ->disable();
    }

    protected function viewItem(): string
    {
        return view('Askit::questions.view.questionWithOptions', ['item' => $this->item])->render();
    }
}