<?php namespace Jcgroep\Askit;

class RadioRenderer extends OptionsRenderer
{
    protected function createItem()
    {
        return QuestionRadioForm::make(null, $this->item)->renderCreateForm();
    }

    protected function editItem()
    {
        return QuestionRadioForm::make(null, $this->item)->renderEditForm();
    }
}