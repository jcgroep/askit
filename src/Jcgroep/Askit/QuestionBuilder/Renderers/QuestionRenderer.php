<?php namespace Jcgroep\Askit;

use Jcgroep\BuildIt\FormElements\FormElement;

class QuestionRenderer extends BaseRenderer
{
    protected function createItem()
    {
        return QuestionForm::make(null, $this->item)->renderCreateForm();
    }

    protected function editItem()
    {
        return QuestionForm::make(null, $this->item)->renderEditForm();
    }

    protected function viewItem(): string
    {
        return view('Askit::questions.view.question', ['item' => $this->item]);
    }

    protected function getElement($class): FormElement
    {
        $element = parent::getElement($class)
            ->withPostfix($this->item->assistant->postfix);
        if($this->item->assistant->isRequired()){
            $element->isRequired();
        }
        return $element;
    }
}