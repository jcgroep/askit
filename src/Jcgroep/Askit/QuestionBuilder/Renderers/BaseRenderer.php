<?php namespace Jcgroep\Askit;

use Input;
use Jcgroep\BuildIt\FormElements\FormElement;
use Jcgroep\BuildIt\FormElements\Text\TextElement;
use Session;

class BaseRenderer
{
    /**
     * @var ShowMode
     */
    protected $mode;
    /**
     * @var QuestionnaireItem
     */
    protected $item;
    /**
     * @var Answer
     */
    protected $answer;

    public function __construct(ShowMode $mode, QuestionnaireItem $item, Answer $answer = null)
    {
        $this->mode = $mode;
        $this->item = $item;
        $this->answer = $answer;
    }

    public function render()
    {
        switch ($this->mode) {
            case ShowMode::CREATE_ITEM:
                return $this->createItem();
            case ShowMode::EDIT_ITEM:
                return $this->editItem();
            case ShowMode::VIEW_ITEM:
                return $this->viewItem();
            case ShowMode::FILL_ITEM:
                return $this->fill();
            case ShowMode::SHOW_ANSWER:
                return $this->show();

            default:
                return $this->show();
        }
    }

    public function __toString(): string
    {
        return $this->render();
    }

    protected function createItem()
    {
        return QuestionnaireItemForm::make(null, $this->item)->renderCreateForm();
    }

    protected function editItem()
    {
        return QuestionnaireItemForm::make(null, $this->item)->renderEditForm();
    }

    protected function viewItem(): string
    {
        return view('Askit::questions.view.baseItem', ['item' => $this->item]);
    }

    public function fill(): FormElement
    {
        return $this->getElement(TextElement::class)->withPlaceholder(trans('Askit::questionnaireItems.defaultPlaceholder'));

    }

    public function show(): FormElement
    {
        return $this->fill()
            ->disable();
    }

    protected function getElement($class): FormElement{
        $formElement = $class::create()
            ->withLabel($this->item->title)
            ->withName($this->item->name)
            ->withRowId($this->item->name)
            ->withLabelClass('col-md-12 question control-label')
            ->withClasses(['col-md-12 answer'])
            ->withDefaultValue(isset($this->answer) ? $this->answer->answer : null);

        $formElement->dependsOnMany($this->item->getDependencies());
        return $formElement;
    }

    public function getPossibleValidators(): array {
        return [];
    }

    public function hasPossibleValidators(): bool {
        return count($this->getPossibleValidators() );
    }
    
    public function canDoDailyTotals() {
        return false;
    }
}