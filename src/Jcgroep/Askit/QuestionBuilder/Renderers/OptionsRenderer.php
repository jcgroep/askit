<?php namespace Jcgroep\Askit;

use Jcgroep\BuildIt\FormElements\FormElement;
use Jcgroep\BuildIt\FormElements\Groups\RadioElement;

class OptionsRenderer extends QuestionRenderer
{
    protected function viewItem(): string
    {
        return view('Askit::questions.view.questionWithOptions', ['item' => $this->item]);
    }

    public function fill(): FormElement{
        $element = $this->getElement(RadioElement::class)
            ->withCollection($this->item->assistant->options, 'label');

        if(QuestionItemLayout::getValue($this->item, QuestionItemLayout::ORIENTATION, RadioOrientation::VERTICAL) == RadioOrientation::VERTICAL){
            $element->oneOptionPerLine();
        }

        if(QuestionItemLayout::getValue($this->item, QuestionItemLayout::USE_OTHER, false) == true){
            $element->withOther();
        }
        return $element;
    }
}