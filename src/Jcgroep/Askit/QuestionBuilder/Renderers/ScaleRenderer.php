<?php namespace Jcgroep\Askit;

use Jcgroep\BuildIt\FormElements\FormElement;
use Jcgroep\BuildIt\FormElements\Text\ScaleElement;
use Jcgroep\BuildIt\FormElements\Text\TextElement;

class ScaleRenderer extends OptionsRenderer
{
    public function fill(): FormElement
    {
        return $this->getElement(ScaleElement::class)
            ->withOptions($this->item->assistant->getOptionArray());
    }
}