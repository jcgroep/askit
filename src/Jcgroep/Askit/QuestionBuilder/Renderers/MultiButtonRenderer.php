<?php namespace Jcgroep\Askit;

use Jcgroep\BuildIt\FormElements\FormElement;
use Jcgroep\BuildIt\FormElements\Groups\ButtonsElement;

class MultiButtonRenderer extends OptionsRenderer
{
    protected function createItem()
    {
        return QuestionForm::make(null, $this->item)
            ->withoutPostfix()
            ->renderCreateForm();
    }

    protected function editItem()
    {
        return QuestionForm::make(null, $this->item)
            ->withoutPostfix()
            ->renderEditForm();
    }

    public function fill(): FormElement
    {
        $element = $this->getElement(ButtonsElement::class)
            ->withCollection($this->item->assistant->options, 'label');

        return $element;
    }

}