<?php namespace Jcgroep\Askit;

use Jcgroep\BuildIt\FormElements\FormElement;
use Jcgroep\BuildIt\FormElements\Groups\SelectElement;

class SelectRenderer extends OptionsRenderer
{
    public function fill(): FormElement{
        return $this->getElement(SelectElement::class)
                ->withoutSearch()
                ->withCollection($this->item->assistant->options, 'label');
        }
}