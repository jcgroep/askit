<?php namespace Jcgroep\Askit;

use Jcgroep\Askit\QuestionValidationType;
use Jcgroep\BuildIt\FormElements\FormElement;

class NumberRenderer extends QuestionRenderer
{
    public function getPossibleValidators(): array {
        return [QuestionValidationType::NONE,
                QuestionValidationType::MIN,
                QuestionValidationType::MAX,
                QuestionValidationType::BETWEEN];
    }

    protected function getElement($class): FormElement
    {
        return parent::getElement($class)
            ->withInputClass('number')
            ->forType('number');
    }

    public function canDoDailyTotals() {
        return true;
    }
}