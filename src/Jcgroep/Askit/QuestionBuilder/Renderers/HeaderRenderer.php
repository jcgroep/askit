<?php namespace Jcgroep\Askit;

use Jcgroep\BuildIt\FormElements\FormElement;
use Jcgroep\BuildIt\FormElements\HeaderElement;

class HeaderRenderer extends QuestionRenderer
{
    protected function createItem()
    {
        return SubtitleForm::make(null, $this->item)->renderCreateForm();
    }

    protected function editItem()
    {
        return SubtitleForm::make(null, $this->item)->renderEditForm();
    }

    public function fill(): FormElement
    {
        return $this->getElement(HeaderElement::class);
    }
}