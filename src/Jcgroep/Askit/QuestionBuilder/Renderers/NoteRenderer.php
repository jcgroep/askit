<?php namespace Jcgroep\Askit;

use Jcgroep\BuildIt\FormElements\FormElement;
use Jcgroep\BuildIt\FormElements\NoteElement;

class NoteRenderer extends QuestionRenderer
{
    protected function createItem()
    {
        return NoteForm::make(null, $this->item)->renderCreateForm();
    }

    protected function editItem()
    {
        return NoteForm::make(null, $this->item)->renderEditForm();
    }

    public function fill(): FormElement
    {
        return $this->getElement(NoteElement::class)
            ->withLabelClass('note note-info');
    }

    protected function viewItem(): string
    {
        return view('Askit::questions.view.note', ['item' => $this->item]);
    }
}