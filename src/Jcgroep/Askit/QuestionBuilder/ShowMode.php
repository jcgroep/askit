<?php namespace Jcgroep\Askit;

class ShowMode extends \Enum
{
    const CREATE_ITEM = 'create';
    const VIEW_ITEM = 'view';
    const EDIT_ITEM = 'edit';
    const FILL_ITEM = 'fill';
    const SHOW_ITEM = 'show';
    const SHOW_ANSWER = 'answer';
}