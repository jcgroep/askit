<?php namespace Jcgroep\Askit;

use Illuminate\Routing\Controller;
use Input;
use Lang;

class AskitQuestionnaireOptionsController extends Controller
{

    public function create()
    {
        return OptionsForm::make(null, new QuestionOption(['question_id' => Input::get('questionId') ,'calculation_id' => Input::get('calculation_id')]))->renderCreateForm();
    }

    public function store()
    {
        $option = QuestionOption::createWithRelations(Input::all());
        if (Input::get('create-other', false) == true) {
            return [
                'message' => trans('Askit::questionnaireOptions.createSuccessCreateNewDirectly'),
                'resetForm' => true
            ];
        }

        $option->question->item->questionnaire->triggerAnswerCache();

        return [
            'message' => trans('Askit::questionnaireOptions.createSuccess'),
            'replaceSelector' => '#question-details',
            'replaceInner' => true,
            'replaceHtml' => (new AskitQuestionnaireItemsController())->show($option->question != null ? $option->question->item : $option->calculation->item )->render(),
            'closeModal' => true,
        ];
    }

    public function edit($id)
    {
        $item = QuestionOption::findOrFail($id);
        return OptionsForm::make(null, $item)->renderEditForm();
    }

    public function update($id)
    {
        $option = QuestionOption::findOrFail($id);
        $option->update(Input::all());

        $item = $option->question_id ? $option->question->item : $option->calculation->item;
        $item->questionnaire->triggerAnswerCache();
        return [
            'message' => trans('Askit::questionnaireOptions.updateSuccess'),
            'replaceSelector' => '#question-details',
            'replaceInner' => true,
            'replaceHtml' => (new AskitQuestionnaireItemsController())->show($item)->render()
        ];
    }

    public function destroy($id)
    {
        $option = QuestionOption::findOrFail($id);
        $item = $option->question ? $option->question->item : $option->calculation->item;
        $option->delete();

        return [
            'message' => trans('Askit::questionnaireOptions.deleteSuccess'),
            'replaceSelector' => '#question-details',
            'replaceInner' => true,
            'replaceHtml' => (new AskitQuestionnaireItemsController())->show($item)->render()
        ];
    }

    public function moveUp(QuestionOption $option)
    {
        $option->moveUp();
        return (new AskitQuestionnaireItemsController())->show($option->question->item)->render();
    }

    public function moveDown(QuestionOption $option)
    {
        $option->moveDown();
        return (new AskitQuestionnaireItemsController())->show($option->question->item)->render();
    }
}
