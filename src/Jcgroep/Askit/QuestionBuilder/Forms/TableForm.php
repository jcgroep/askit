<?php

namespace Jcgroep\Askit;

use Illuminate\Support\Facades\Lang;
use Jcgroep\BuildIt\Form;
use Jcgroep\BuildIt\FormElements\Text\TextElement;
use Jcgroep\BuildIt\FormElements\Text\TextAreaElement;

class TableForm extends QuestionnaireItemForm
{
    protected function renderForm(Form $form)
    {
        parent::renderForm($form);

        $form->addElement(TextAreaElement::create()
            ->withName('layout_table_definition' )
            ->withDefaultValue(QuestionItemLayout::getValue($this->target, 'layout_table_definition'))
            ->withLabel(trans('Askit::questionnaires.table.definition'))
            ->withClasses(['col-md-8'])
            ->withLabelClass('col-md-4 control-label')
        );

        $form->addElement(TextElement::create()
            ->withName('layout_table_rows' )
            ->withDefaultValue(QuestionItemLayout::getValue($this->target, 'layout_table_rows'), 5)
            ->withLabel(trans('Askit::questionnaires.table.rows'))
            ->withClasses(['col-md-4'])
            ->withLabelClass('col-md-4 control-label')
        );        

        return $form;
    }
}