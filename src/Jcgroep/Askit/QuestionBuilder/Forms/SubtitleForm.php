<?php namespace Jcgroep\Askit;

use Jcgroep\BuildIt\Form;
use Jcgroep\BuildIt\FormElements\Text\TextElement;
use Lang;

class SubtitleForm extends QuestionnaireItemForm
{
    protected function renderForm(Form $form)
    {
        return $form->addElement(TextElement::create()
            ->withName('title')
            ->withDefaultValue($this->target->title)
            ->withLabel(trans('Askit::questionnaires.Title'))
            ->withClasses(['col-md-8'])
            ->isRequired()
            ->withLabelClass('col-md-4 control-label')
        );
    }
}