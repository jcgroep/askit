<?php namespace Jcgroep\Askit;

use Jcgroep\BuildIt\Form;
use Jcgroep\BuildIt\FormElements\CheckboxElement;
use Jcgroep\BuildIt\FormElements\Groups\BooleanElement;
use Jcgroep\BuildIt\FormElements\Groups\RadioElement;
use Jcgroep\BuildIt\FormElements\Text\TextAreaElement;
use Lang;

class QuestionRadioForm extends QuestionnaireItemForm
{
    protected function renderForm(Form $form)
    {
        parent::renderForm($form);

        $form->addElement(BooleanElement::create()
            ->withName('required')
            ->isRequired()
            ->withDefaultValue($this->target->assistant != null ? $this->target->assistant->required : null)
            ->withLabel(trans('Askit::form.required'))
            ->withClasses(['col-md-8'])
            ->withLabelClass('col-md-4 control-label'));

        $form->addElement(RadioElement::create()
            ->withName(QuestionItemLayout::ORIENTATION)
            ->withDefaultValue(QuestionItemLayout::getValue($this->target, QuestionItemLayout::ORIENTATION, RadioOrientation::VERTICAL))
            ->withLabel(trans('Askit::questionnaireItems.orientation'))
            ->withSimpleOptions(RadioOrientation::all(), 'Askit::questionnaires.options')
            ->withClasses(['col-md-8'])
            ->withLabelClass('col-md-4 control-label'));

        $form->addElement(CheckboxElement::create()
            ->withName(QuestionItemLayout::USE_OTHER)
            ->withDefaultValue(QuestionItemLayout::getValue($this->target, QuestionItemLayout::USE_OTHER, false))
            ->withLabel(trans('Askit::questionnaireItems.useOther'))
            ->withLabelClass('col-md-4 control-label')
        );

        $form->addElement(CheckboxElement::create()
            ->withName(QuestionItemLayout::SHOW_OPTION_VALUE)
            ->withDefaultValue(QuestionItemLayout::getValue($this->target, QuestionItemLayout::SHOW_OPTION_VALUE, false))
            ->withLabel(trans('Askit::questionnaireItems.showOptionValue'))
            ->withLabelClass('col-md-4 control-label')
        );        

        return $form;
    }
}