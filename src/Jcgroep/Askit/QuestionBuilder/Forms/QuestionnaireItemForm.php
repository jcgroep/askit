<?php namespace Jcgroep\Askit;

use Jcgroep\BuildIt\AbstractForm;
use Jcgroep\BuildIt\Form;
use Jcgroep\BuildIt\FormElements\Groups\BooleanElement;
use Jcgroep\BuildIt\FormElements\Groups\SelectElement;
use Jcgroep\BuildIt\FormElements\Text\HiddenInput;
use Jcgroep\BuildIt\FormElements\Text\TextElement;
use Jcgroep\BuildIt\Modal;
use Lang;

class QuestionnaireItemForm extends AbstractForm
{

    public function renderEditForm(): Form
    {
        $form = Form::make(['askit.item.update', $this->target->id], 'PUT', $this->errors)
            ->isAjax()
            ->withId('edit-questionnaire-item-form');
        $form->inModal(Modal::create(trans('Askit::questionnaires.editQuestionItem'), 'edit-question-option', $form));
        $this->addRequiredFields($form);
        return $this->renderForm($form);
    }

    public function renderCreateForm(): Form
    {
        $form = Form::make('askit.item.store', 'POST', $this->errors)
            ->withId('create-questionnaire-item-form')
            ->isAjax();
        $form->inModal(Modal::create(trans('Askit::questionnaires.createQuestionItem'), 'edit-question-option', $form));
        $this->addRequiredFields($form);
        return $this->renderForm($form);
    }

    protected function renderForm(Form $form)
    {
        $form->addElement(TextElement::create()
            ->withName('title')
            ->withDefaultValue($this->target->title)
            ->withLabel(trans('Askit::questionnaires.Question'))
            ->withClasses(['col-md-8'])
            ->isRequired()
            ->withLabelClass('col-md-4 control-label'));

        $form->addElement(BooleanElement::create()
            ->withName('show_in_excerpt')
            ->isRequired()
            ->withDefaultValue((bool) $this->target->show_in_excerpt)
            ->withLabel(trans('Askit::questionnaires.ShowInExerpt'))
            ->withClasses(['col-md-8'])
            ->withLabelClass('col-md-4 control-label'));
        return $form;
    }

    protected function getUpdateUrl()
    {

        if ($this->target->id == null) {
            return route('askit.item.create', ['questionnaire_id' => $this->target->questionnaire_id]);
        }
        return route('askit.item.edit', [$this->target->id]);
    }

    private function addRequiredFields(Form $form)
    {
        $form->addElement(HiddenInput::create()
            ->withName('questionnaire_id')
            ->withDefaultValue($this->target->questionnaire_id));

        if ($this->target->id == null) {
            $allowedTypes = QuestionType::grouped();
        } elseif ($this->target->assistant_type == Question::class) {
            $allowedTypes = QuestionType::onlyQuestions();
        } else {
            $allowedTypes = QuestionType::onlyCalculations();
        }

        $form->addElement($type = SelectElement::create()
            ->withName('type')
            ->isRequired()
            ->hideEmpty()
            ->withLabel(trans('Askit::questionnaires.Type'))
            ->withDefaultValue($this->target->type)
            ->withDataAttribute('update-ajax', 'update-ajax')
            ->withDataAttribute('target', '#' . $form->getId())
            ->withDataAttribute('url', $this->getUpdateUrl())
            ->withSimpleOptions($allowedTypes, 'Askit::questionnaires.type')
            ->withClasses(['col-md-3'])
            ->withLabelClass('col-md-4 control-label'));
    }
}