<?php namespace Jcgroep\Askit;


use Jcgroep\BuildIt\AbstractForm;
use Jcgroep\BuildIt\Form;
use Jcgroep\BuildIt\FormElements\CheckboxElement;
use Jcgroep\BuildIt\FormElements\Groups\SelectElement;
use Jcgroep\BuildIt\FormElements\Text\HiddenInput;
use Jcgroep\BuildIt\FormElements\Text\TextAreaElement;
use Jcgroep\BuildIt\FormElements\Text\TextElement;
use Jcgroep\BuildIt\Modal;
use Jcgroep\ConfigureIt\Settings\ValueObjects\DropDownOption;
use Lang;

class OptionsForm extends AbstractForm
{
    public function renderEditForm()
    {
        $form = Form::make(['askit.question-option.update', $this->target->id], 'PUT', $this->errors);
        $form->inModal(Modal::create(trans('Askit::questionnaireOptions.Edit'), 'edit-question-option', $form))
            ->withDataAttribute('close-after', 1);

        return $this->renderForm($form);
    }

    public function renderCreateForm()
    {
        $form = Form::make('askit.question-option.store', 'POST', $this->errors);
        $modal = Modal::create(trans('Askit::questionnaireOptions.Create'), 'edit-question-option', $form)
            ->withDataAttribute('reload-on-close', 1);
        $form->inModal($modal);

        $this->renderForm($form);

        $form->addElement(CheckboxElement::create()
            ->withName('create-other')
            ->withLabel(trans('Askit::questionnaireOptions.createOther'))
            ->withDefaultValue(1)
        );
        return $form;
    }

    protected function renderForm(Form $form)
    {
        $form->isAjax();

        $availableCompareMethods = DependencyOperator::all();

        $form->addElement(HiddenInput::create()
            ->withName('question_id')
            ->withDefaultValue($this->target->question_id));

        $form->addElement(HiddenInput::create()
            ->withName('calculation_id')
            ->withDefaultValue($this->target->calculation_id));

        if ($this->target->calculation_id) {
            $form->addElement(SelectElement::create()
                ->withName('operator')
                ->withDefaultValue($this->target->operator)
                ->withArray($availableCompareMethods)
                ->withLabel(trans('Askit::questionnaires.operator'))
                ->isRequired());
        }


        $form->addElement(TextElement::create()
            ->withName('value')
            ->withDefaultValue($this->target->value)
            ->withLabel(trans('Askit::questionnaires.value'))
            ->isRequired());

        $form->addElement(TextElement::create()
            ->withName('label')
            ->withDefaultValue($this->target->label)
            ->withLabel(trans('Askit::questionnaires.label'))
            ->isRequired());


        return $form;
    }




}