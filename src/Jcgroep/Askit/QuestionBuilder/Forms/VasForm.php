<?php

namespace Jcgroep\Askit;

use Illuminate\Support\Facades\Lang;
use Jcgroep\BuildIt\Form;
use Jcgroep\BuildIt\FormElements\Text\TextElement;

class VasForm extends QuestionnaireItemForm
{
    protected function renderForm(Form $form)
    {
        parent::renderForm($form);

        $form->addElement(TextElement::create()
            ->withName('layout_scale_prefix')
            ->withDefaultValue(QuestionItemLayout::getValue($this->target, QuestionItemLayout::SLIDER_PREFIX, trans('Askit::questionnaires.scale.defaultMinimumDescription')))
            ->withLabel(trans('Askit::questionnaires.scale.minimumDescription'))
            ->withClasses(['col-md-8'])
            ->withLabelClass('col-md-4 control-label')
        );

        $form->addElement(TextElement::create()
            ->withName('layout_scale_postfix')
            ->withDefaultValue(QuestionItemLayout::getValue($this->target, QuestionItemLayout::SLIDER_POSTFIX, trans('Askit::questionnaires.scale.defaultMaximumDescription')))
            ->withLabel(trans('Askit::questionnaires.scale.maximumDescription'))
            ->withClasses(['col-md-8'])
            ->withLabelClass('col-md-4 control-label')
        );

        return $form;
    }
}