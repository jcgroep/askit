<?php namespace Jcgroep\Askit;

use Jcgroep\BuildIt\Form;
use Jcgroep\BuildIt\FormElements\Groups\BooleanElement;
use Jcgroep\BuildIt\FormElements\Groups\SelectElement;
use Jcgroep\BuildIt\FormElements\Text\TextElement;
use Jcgroep\BuildIt\FormElements\Groups\ButtonsElement;
use Lang;

class QuestionForm extends QuestionnaireItemForm
{
    protected $postfix = true;

    protected function renderForm(Form $form)
    {
        parent::renderForm($form);

        $form->addElement(ButtonsElement::create()
            ->withName('required')
            ->isRequired()
            ->withDefaultValue($this->target->assistant != null ? (bool) $this->target->assistant->required : false)
            ->withLabel(trans('Askit::form.required'))
            ->withClasses(['col-md-8'])
            ->withLabelClass('col-md-4 control-label')
            ->addOption(trans('global.Yes'), 1)
            ->addOption(trans('global.No'), 0)
        );

        if ($this->postfix) {
            $form->addElement(TextElement::create()
                ->withName('postfix')
                ->withDefaultValue($this->target->assistant != null ? $this->target->assistant->postfix : null)
                ->withLabel(trans('Askit::form.postfix'))
                ->withClasses(['col-md-4'])
                ->withLabelClass('col-md-4 control-label'));
        }

        $item = new QuestionnaireItem(['type' => $this->target->type]);
        $renderer = QuestionnaireItemRendererFactory::build(new ShowMode(ShowMode::FILL_ITEM), $item);

        $validator = $this->target->assistant != null ? $this->target->assistant->validators : QuestionValidationType::NONE;

        if ($renderer->canDoDailyTotals() ) {
            $form->addElement(ButtonsElement::create()
                ->withName('show_total')
                ->isRequired()
                ->withDefaultValue($this->target->assistant != null ? (bool) $this->target->assistant->show_total : false)
                ->withLabel(trans('Askit::form.show_total'))
                ->withClasses(['col-md-8'])
                ->withLabelClass('col-md-4 control-label')
                ->addOption(trans('global.Yes'), 1)
                ->addOption(trans('global.No'), 0)
            );            
        }
        if ($renderer->hasPossibleValidators()) {
            $form->addElement(SelectElement::create()
                ->withName('validators')
                ->withDefaultValue($validator)
                ->withLabel(trans('Askit::form.validators'))
                ->withClasses(['col-md-8'])
                ->withDataAttribute('update-ajax', 'update-ajax')
                ->withDataAttribute('target', '#' . $form->getId())
                ->withDataAttribute('url', $this->getUpdateUrl())
                ->withLabelClass('col-md-4 control-label')
                ->withSimpleOptions($renderer->getPossibleValidators(),
                    'Askit::questionnaires.validators')
            );

            foreach (QuestionValidationType::getAdditionalFieldTranslations($validator) as $index => $translation) {
                $name = 'validator_config' . $index;
                $form->addElement(TextElement::create()
                    ->withName($name)
                    ->withDefaultValue($this->target->assistant != null ? $this->target->assistant[$name] : null)
                    ->withLabel($translation)
                    ->withClasses(['col-md-2'])
                    ->withLabelClass('col-md-4 control-label')
                );
            }
        }

        return $form;
    }

    public function withoutPostfix()
    {
        $this->postfix = false;
        return $this;
    }
}