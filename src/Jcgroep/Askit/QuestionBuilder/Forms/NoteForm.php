<?php namespace Jcgroep\Askit;

use Jcgroep\BuildIt\Form;
use Jcgroep\BuildIt\FormElements\Text\TextAreaElement;
use Lang;

class NoteForm extends QuestionnaireItemForm
{
    protected function renderForm(Form $form)
    {
        return $form->addElement(TextAreaElement::create()
            ->withName('title')
            ->withDefaultValue($this->target->title)
            ->withLabel(trans('Askit::questionnaires.Text'))
            ->withSummerNote()
            ->withClasses(['col-md-8'])
            ->isRequired()
            ->withLabelClass('col-md-4 control-label')
        );
    }
}