<?php namespace Jcgroep\Askit;

use Calculation;
use Illuminate\Support\Collection;
use Input;
use Jcgroep\BuildIt\Form;
use Jcgroep\BuildIt\FormElements\CustomElement;
use Jcgroep\BuildIt\FormElements\Groups\CheckboxGroupElement;
use Jcgroep\BuildIt\FormElements\Groups\SelectElement;
use Jcgroep\BuildIt\FormElements\SimpleDropdownElement;
use Lang;

class CalculationForm extends QuestionnaireItemForm
{
    protected function renderForm(Form $form)
    {
        parent::renderForm($form);

        $calculationType = $this->target->assistant->calculation_type ?? CalculationType::all();
        $form->addElement(SelectElement::create()
            ->withName('calculation_type')
            ->withDefaultValue($calculationType)
            ->withLabel(trans('Askit::form.calculationType'))
            ->withClasses(['col-md-8'])
            ->withLabelClass('col-md-4 control-label')
            ->withSimpleOptions(CalculationType::all(), 'Askit::questionnaires.calculation_type')
            ->withDataAttribute('update-ajax', 'update-ajax')
            ->withDataAttribute('target', '#' . $form->getId())
            ->withDataAttribute('url', $this->getUpdateUrl())
        );

        switch ($calculationType) {
            case CalculationType::SELECTION:
                $this->addFunctionSelection($form);
                $this->addSelectionQuestions($form);
                break;
            case CalculationType::CUSTOM:
                $this->addCustomCalculationInput($form);
                break;
            default:
                $this->addFunctionSelection($form);
                break;
        }

        return $form;
    }

    private function addFunctionSelection(Form $form)
    {
        $form->addElement(SimpleDropdownElement::create()
            ->withLabel(trans('Askit::calculations.function'))
            ->withLabelClass('col-md-4 control-label')
            ->withName('operator')
            ->withOptions(CalculationOperatorType::all())
            ->withDefaultValue(isset($this->target->assistant) ? $this->target->assistant->operator : CalculationOperatorType::SUM)
        );
    }

    private function addSelectionQuestions(Form $form)
    {
        $questionnaire = Questionnaire::findOrFail($this->target->questionnaire_id);
        $questions = QuestionnaireItem::whereIn('id',
            $questionnaire->items->onlyUsefulForCalculations()
                ->where('id', '!=', $this->target->id)
                ->pluck('id'))
            ->get();
        $form->addElement(CheckboxGroupElement::create()
            ->withLabel(trans('Askit::questionnaireItems.Questions'))
            ->withLabelClass('col-md-4 control-label')
            ->withClasses(['col-md-8'])
            ->withName('question_selection')
            ->withCollection($questions, 'title')
            ->withDefaultValue(isset($this->target->assistant) ? $this->target->assistant->items : new Collection())
        );
    }

    private function addCustomCalculationInput(Form $form)
    {
        $questionnaire = Questionnaire::findOrFail($this->target->questionnaire_id);
        $calculationItem = $this->target;
        $form->addElement(CustomElement::create()
            ->withLabel(trans('Askit::form.calculation'))
            ->withClasses(['col-md-8'])
            ->withLabelClass('col-md-4 control-label')
            ->withHtml(view('Askit::calculations.form.customCalculation', compact('questionnaire', 'calculationItem'))->render()));
    }
}