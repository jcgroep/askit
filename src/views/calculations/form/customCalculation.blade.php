<div class="btn-group pull-right">
	<a href="#" class="btn blue dropdown-toggle pull-right" id="toggle-training-actions" data-toggle="dropdown"
	   aria-expanded="true" style="z-index: 1000;">
		{{trans("actions.Actions")}} <i class="fa fa-angle-down"></i>
	</a>

	<ul class="dropdown-menu theme-panel no-padding" style="min-width: 50px; max-width: 600px; position: relative">
		@foreach($questionnaire->items->onlyUsefulForCalculations()->where('id', '!=', $calculationItem->id) as $item)
			<li>
				<button class="lookLikeLink insert-question" data-item-id="{{$item->id}}">
					{{StringObject::make($item->title)->substr(0,70, true)}}
				</button>
			</li>
		@endforeach
	</ul>
</div>

<div style="padding: 5px; width: 100%; height: 80px; border: 1px solid black; clear: both; cursor: text;">
	<div id="editable-calculation" class="calculation_string" contenteditable="true" style="display: inline-block; outline: 0;">
		{!!  $calculationItem->assistant != null ? $calculationItem->assistant->getHumanReadableCalculationString() : '' !!}
	</div>
</div>
<textarea name="custom_calculation_string" id="custom-calculation-textarea" style="display: none">
	{!! $calculationItem->assistant != null ? $calculationItem->assistant->getHumanReadableCalculationString() : '' !!}
</textarea>
<script>
	function isOrContains(node, container) {
		while (node) {
			if (node === container) {
				return true;
			}
			node = node.parentNode;
		}
		return false;
	}

	function elementContainsSelection(el) {
		var sel;
		if (window.getSelection) {
			sel = window.getSelection();
			if (sel.rangeCount == 0 || !isOrContains(sel.getRangeAt(0).commonAncestorContainer, el)) {
				return false;
			}
			return true;
		} else if ((sel = document.selection) && sel.type != "Control") {
			return isOrContains(sel.createRange().parentElement(), el);
		}
		return false;
	}

	function placeCaretAtEnd(el) {
		var range;
		range = document.createRange();
		range.selectNodeContents(el);
		range.collapse(false);
		var sel = window.getSelection();
		sel.removeAllRanges();
		sel.addRange(range);
		return range;
	}

	$('.insert-question').on('click', function (e) {
		e.preventDefault();
		var range;
		var editor = document.getElementById('editable-calculation');
		if (elementContainsSelection(editor)) {
			range = window.getSelection().getRangeAt(0);
		} else {
			range = placeCaretAtEnd(editor);
		}

		var element = document.createElement("span");
		element.appendChild(document.createTextNode($(this).text()));
		element.setAttribute("data-item-id", $(this).data('item-id'));
		element.setAttribute("contenteditable", "false");
		element.setAttribute("class", "item");
		range.insertNode(element);
		range.collapse(false);
		editor.focus();
		placeCaretAtEnd(element);
		$('#editable-calculation').trigger('change');
	});

	$('#editable-calculation').on('change blur keyup paste input', function () {
		$('#custom-calculation-textarea').val($(this).html());
	});
</script>
