@extends('Askit::questions.view.baseItem')

@section('additionalRows')
	@if ($item->assistant->required !== null)
	<tr>
		<td>@lang('Askit::questionnaires.required')</td>
			<td>@bool($item->assistant->required)</td>
	</tr>
	@endif

	@if ($item->assistant->show_total)
		<tr>
			<td>@lang('Askit::form.show_total')</td>
			<td>@bool($item->assistant->show_total)</td>
		</tr>
	@endif


	@if ($item->assistant->validators)
        <tr>
                <td>@lang('Askit::form.validators')</td>
                <td>@lang('Askit::questionnaires.validators.' . $item->assistant->validators)
                @if ( $item->assistant->validator_config1)
                    ({{ implode(", ", array_filter([$item->assistant->validator_config1, $item->assistant->validator_config2])) }})
                @endif
                </td>
        </tr>
        @endif
	<tr>
		<td>@lang('Askit::form.postfix')</td>
		<td>{{$item->assistant->postfix}}</td>
	</tr>
@endsection