@if($item->questionnaire->status == \Jcgroep\Askit\QuestionnaireStatusType::CONCEPT)
	<div class="btn-group pull-right caption">
		<a href="#" class="btn blue dropdown-toggle" id="toggle-exercise-actions" data-toggle="dropdown" aria-expanded="true">
			{{trans("actions.Actions")}} <i class="fa fa-angle-down"></i>
		</a>

		<div class="dropdown-menu pull-right dropdown-custom">
			<ul class="nav">
				<li>
					<button data-link="{!! route('askit.item.edit', $item->id) !!}" data-target="#ajax-content" data-show-popup="load-ajax">
						@lang('Askit::questionnaires.edit')
					</button>
				</li>
				<li>
					{!! Form::open(['route' => ['askit.item.destroy', $item->id], 'method' => 'delete', 'data-remote', 'style' => 'display: inline'])  !!}
					<button type="submit" data-confirm="@lang('global.AreYouSure')">
						{{ trans('actions.Delete') }}
					</button>
					{!! Form::close()  !!}
				</li>
				<li>
					<a href="{!! route('askit.item.duplicate', $item->id) !!}">
						@lang('Askit::questionnaireItems.duplicate')
					</a>
				</li>
			</ul>
		</div>
	</div>
	<div class="pull-right" style="margin-top: 15px;">


	</div>
@endif


<h2>@lang('Askit::questionnaires.QuestionDetails')</h2>
<table class="table">
	<tr>
		<td style="width: 150px;">@lang('Askit::questionnaires.Question')</td>
		<td>{!! $item->title->stripTags() !!}</td>
	</tr>
	<tr>
		<td>@lang('Askit::questionnaires.typeLabel')</td>
		<td>@lang('Askit::questionnaires.type.' . $item->type)</td>
	</tr>
	<tr>
		<td>@lang('Askit::questionnaires.summary')</td>
		<td>@bool($item->show_in_excerpt)</td>
	</tr>
	@yield('additionalRows')
</table>


@yield('additional')


<h3>
	@choice('Askit::questionnaires.dependencies',2)
	@if($item->questionnaire->status != \Jcgroep\Askit\QuestionnaireStatusType::AVAILABLE)
		<button data-link="{!! route('askit.dependency.create', ['item_id' => $item->id]) !!}" class="btn blue" data-show-popup="load-ajax" data-target="#popup-dependency">
			@lang('Askit::questionnaires.add')
		</button>
	@endif
</h3>
<p>@lang('Askit::questionnaires.dependencyInfo')</p>
<table class="table">

	<thead>
	<tr>
		<th>@lang('Askit::questionnaires.Question')</th>
		<th>@choice('Askit::questionnaires.dependencies',1)</th>
		<th>@lang('Askit::questionnaires.value')</th>
		@if($item->questionnaire->status != \Jcgroep\Askit\QuestionnaireStatusType::AVAILABLE)
			<th style="width: 95px;">@lang('Askit::questionnaires.Actions')</th>
		@endif
	</tr>
	</thead>
	<tbody>
	@foreach($item->dependencies as $dependency)
		<tr>
			<td>{{$dependency->dependend->title  }}</td>
			<td>{{$dependency->getOperator()}}</td>
			<td>{{$dependency->dependend->getHumanReadableAnswerAttribute($dependency->dependend_value)}}</td>
			<td class="no-padding">
				@if($item->questionnaire->status != \Jcgroep\Askit\QuestionnaireStatusType::AVAILABLE)
					{!! Form::open(['route'=> ['askit.dependency.destroy', $dependency->id], 'method' => 'delete', 'role' => 'form', 'data-remote'])  !!}
					<button id="btn-deleteDependency" type="submit" title="@lang('Askit::questionnaires.deleteDependency')" class="fa fa-action-button no-padding no-margin"
					        data-confirm="@lang('Askit::questionnaires.sureDelete')">
						&#xf014;
					</button>
					{!! Form::close()  !!}

					<button data-link="{!! route('askit.dependency.edit', $dependency->id) !!}" class="fa fa-action-button no-padding no-margin" data-target="#ajax-content" data-show-popup="load-ajax"
					        title="@lang('Askit::questionnaires.editDependency')">
						&#xf044;
					</button>`
				@endif

				@if( $dependency->containsLoop() )
					<i class="fa fa-exclamation-triangle" aria-hidden="true" style="color: red; font-size: 120%; margin-top: 8px;" title="@lang("Askit::questionnaires.loopDetected")"></i>
				@endif
			</td>
		</tr>
	@endforeach
	</tbody>
</table>

<div id="popup-dependency"></div>
