@extends('Askit::questions.view.question')

@section('additionalRows')
	@if (\Jcgroep\Askit\QuestionItemLayout::getValue($item, "layout_show_option_value") == true)
		<tr>
			<td>@lang('Askit::questionnaireItems.showOptionValue')</td>
			<td>@bool(true)</td>
		</tr>
	@endif
	@if (\Jcgroep\Askit\QuestionItemLayout::getValue($item, "layout_use_other") == true)
		<tr>
			<td>@lang('Askit::questionnaireItems.useOther')</td>
			<td>@bool(true)</td>
		</tr>
	@endif
@endsection

@section('additional')
	<h3>
		@lang('Askit::questionnaires.option')
		@if($item->questionnaire->status != \Jcgroep\Askit\QuestionnaireStatusType::AVAILABLE)
			@if ($item->assistant_type == 'Calculation')
					<button data-link="{!! route('askit.question-option.create', ['calculation_id' => $item->assistant->id]) !!}" class="btn blue" data-show-popup="load-ajax" data-target="#popup-container">
						@lang('Askit::questionnaires.add')
					</button>
				@else
					<button data-link="{!! route('askit.question-option.create', ['questionId' => $item->assistant->id]) !!}" class="btn blue" data-show-popup="load-ajax" data-target="#popup-container">
						@lang('Askit::questionnaires.add')
					</button>
			@endif
		@endif
	</h1>

	<table class="table table-striped">
		<thead>
		<tr>
			<th>@lang('Askit::questionnaires.value')</th>
			<th>@lang('Askit::questionnaires.label')</th>
			@if($item->questionnaire->status != \Jcgroep\Askit\QuestionnaireStatusType::AVAILABLE)
				<th style="width: 95px;">@lang('Askit::questionnaires.Actions')</th>
			@endif
		</tr>
		</thead>
		<tbody>
		@foreach($item->assistant->options as $option)
			<tr data-toggle-element=".fa-action-button">
				<td>{{$option->operator}} {{$option->value}}</td>
				<td>{{$option->label}}</td>
				<td class="no-padding">
				@if($item->questionnaire->status != \Jcgroep\Askit\QuestionnaireStatusType::AVAILABLE)
					{!! Form::open(['route'=> ['askit.question-option.destroy', $option->id], 'method' => 'delete', 'role' => 'form', 'data-remote'])  !!}
					<button type="submit" title="@lang('Askit::questionnaires.deleteOption')" class="fa fa-action-button no-padding no-margin" style="display: none" data-confirm="@lang('Askit::questionnaires.sureDelete')">
						&#xf014;
					</button>
					{!! Form::close()  !!}
					<button data-link="{!! route('askit.question-option.edit', $option->id) !!}" class="fa fa-action-button no-padding no-margin" style="display: none" data-target="#ajax-content"
					        data-show-popup="load-ajax">
						&#xf044;
					</button>
					<button class="fa fa-action-button no-padding no-margin {{$option->isFirst() ? 'disabledElement' : ''}}" style="width: 15px; display: none" data-ajax data-target="#question-details"
					        href="{!! route('askit.questionOption.moveUp', $option->id) !!}">
						&#xf01b;
					</button>
					<button class="fa fa-action-button no-padding no-margin {{$option->isLast() ? 'disabledElement' : ''}}" style="width: 15px; display: none" data-ajax data-target="#question-details"
					        href="{!! route('askit.questionOption.moveDown', $option->id) !!}">
						&#xf01a;
					</button>
				@endif
				</td>
			</tr>
		@endforeach
		</tbody>
	</table>


	<div id="popup-container"></div>
@endsection