@extends('Askit::questions.view.baseItem')

@section('additionalRows')
	<tr>
		<td>@lang('Askit::calculations.calculation')</td>
		<td class="calculation_string">{!! $item->assistant->getHumanReadableCalculationString() !!}</td>
	</tr>
@endsection