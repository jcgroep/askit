
@if ($items->count() > 2)
	<div class="note note-warning p-3 m-0">Deze vragenlijst heeft een groot aantal vragen. Dit maakt het beantwoorden voor de patiënt minder ideaal.</div>
@endif

<div id="questionnaire-item-overview">
	<table class="table">
		<thead>
		<tr>
			<th>@lang('Askit::questionnaires.Question')</th>
		</tr>
		</thead>
		<tbody @if($questionnaire->status == \Jcgroep\Askit\QuestionnaireStatusType::CONCEPT) data-sortable @endif data-url="{!! route('askit.item.changeOrder', $questionnaire->id) !!}">
		@foreach($items as $item)
			<tr class="item-row {{(isset($selectedQuestionItem) && $selectedQuestionItem->id == $item->id) ? 'active' : ''}}" data-toggle-element=".fa-action-button" data-instance-id="{!! $item->id !!}">
				<td class="show-questionnaire-item" data-item-id="{{$item->id}}">
					@if ($item->isMissingOptions())
						<i class="fa fa-exclamation-triangle" style="color: red" aria-hidden="true" title="Deze vraag mist antwoordopties en kan niet beantwoord worden"></i>
					@endif
					{!! $item->shortTitle !!}
						<div class="pull-right">
							<i class="fa fa-arrows-v handle" aria-hidden="true"></i>
						</div>
				</td>
			</tr>
		@endforeach
		</tbody>
	</table>
</div>
<script>
	initSortable();
</script>