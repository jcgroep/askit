<div class="col-md-6 overview">
	<div class="pull-right" style="margin-top: 16px;">
		<button data-link="{!! route('askit.item.create', ['questionnaire_id' => $questionnaire->id]) !!}" class="btn blue" data-target="#ajax-content" data-show-popup="load-ajax">
			@lang('Askit::questionnaires.newQuestion')
		</button>
	</div>

	<h2>@lang('Askit::questionnaires.ContentOfQuestionnaire')</h2>
	@include('Askit::questionnaires.edit.questionnaireItemsTable')
</div>

<div class="col-md-6" id="question-details">
	@if(isset($selectedQuestionItem))
		{!! (new Jcgroep\Askit\AskitQuestionnaireItemsController())->show($selectedQuestionItem) !!}
	@else
	    <h2>@lang('Askit::questionnaires.QuestionDetails')</h2>
		<p>
			@lang('Askit::questionnaires.clickQuestionForInfo')
		</p>
	@endif
</div>

