@extends('layouts.master')

@section('content')
	<div class="row">
		<div class="col-md-12">
			<div class="portlet light">
				<div class="portlet-title">
					<div class="caption">
						<h1><i class="fa fa-user"></i> {{trans_choice("questionnaires.Participant",2)}}</h1>
					</div>
					<div class="actions btn-set">
						<a class="btn btn-lg btn-circle blue" data-toggle="modal" href="#participant-create">
							<i class="fa fa-plus"></i> {{trans("user.NewPatient")}}
						</a>
					</div>
				</div>
				<div class="portlet-body">
					<table class="table table-striped table-bordered table-hover">
						<tr>
							<th>@lang('questionnaires.Name')</th>
							<th>@lang('questionnaires.Number')</th>
							<th>@lang('questionnaires.TotalTests')</th>
							<th>@lang('questionnaires.OpenTests')</th>
						</tr>
						@foreach( $participants as $participant)
							<tr>
								<td>{!! Html::linkRoute('participants.show', $participant->fullName, $participant->id)  !!}</td>
								<td>{{$participant->patient_number}}</td>
								<td>{{$participant->getNumberOfTests() }}</td>
								<td>{{$participant->getNumberOfOpenTests() }}</td>
							</tr>
						@endforeach
					</table>
				</div>
			</div>
		</div>
	</div>
	{!! Jcgroep\Askit\Forms\ParticipantForm::make($errors->participant, new Participant())->renderCreateForm() !!}
@endsection