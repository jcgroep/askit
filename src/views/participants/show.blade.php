@extends('layouts.master')

@section('content')
	<div class="row" xmlns="http://www.w3.org/1999/html">
		<div class="row">
			<div class="col-md-12">
				<div class="portlet light">
					<h1>
						<a class="btn blue" href="{{URL::route('participants.index')}}">
							<i class="fa fa-arrow-left"></i>
							{{ucfirst(trans('actions.back'))}}
						</a>
						{{ $participant->fullName }}
						<a class="btn btn-circle btn-default" data-toggle="modal" href="#patient-edit">
							<i class="fa fa-pencil"></i>
				            <span class="hidden-480">
			                    {{ trans("actions.EditDetails") }}
				            </span>
						</a>
					</h1>

					<div class="row">
						<div class="col-md-5">
							<table class="table">
								<tbody>
								<tr>
									<td>{{ trans('questionnaires.Name') }}</td>
									<td>{{$participant->fullName}}</td>
								</tr>
								<tr>
									<td>{{ trans('questionnaires.Number') }}</td>
									<td>{{$participant->patient_number}}</td>
								</tr>
								<tr>
									<td>{{ trans('questionnaires.Email') }}</td>
									<td>{{$participant->user->email}}</td>
								</tr>
								<tr>
									<td>{{ trans('questionnaires.Comments') }}</td>
									<td>{{$participant->user->comments}}</td>
								</tr>

								</tbody>
							</table>
						</div>
					</div>

					<h2>
						{{ trans_choice('questionnaires.Questionnaires',2) }}
						<a class="btn btn-circle btn-default" data-toggle="modal" href="#assign-questionnaire">
							<i class="fa fa-plus"></i>
				            <span class="hidden-480">
			                    {{ trans("questionnaires.NewQuestionnaire") }}
				            </span>
						</a>
					</h2>
					<table class="table table-striped table-bordered table-hover">
						<tr>
							<th>{{ trans('questionnaires.Name') }}</th>
							<th>{{ trans('questionnaires.Date') }}</th>
							<th>{{ trans('questionnaires.Status') }}</th>
							@if($participant->user->email != null)
								<th></th>
							@endif
						</tr>
						@foreach( $tests as $test)
							<tr>
								<td>{!! Html::linkRoute('tests.show', $test->questionnaire->name, ['id' => $test->id], ['data-show-popup', 'data-target' => '#show-task'])  !!}</td>
								<td>{{$test->created_at}}</td>
								<td>{{ trans_choice("questionnaires.ParticipantTestStatus", $test->status ) }}</td>
								@if($participant->user->email != null)
								<td>
									{!! Html::linkRoute('tests.sendReminder', isset($test->hash) ? trans('questionnaires.sendReminder') : trans('questionnaires.sendQuestionnaire'), [$test->id]) !!}
								</td>
								@endif
							</tr>
						@endforeach
					</table>
				</div>
			</div>
		</div>
	</div>

	{!! Jcgroep\Askit\Forms\ParticipantForm::make($errors->participant, $participant)->renderEditForm() !!}
	{!! Jcgroep\Askit\Forms\AssignQuestionnaireForm::make($errors->patient, $participant)->renderEditForm() !!}
@endsection

