@extends('export.pdf.master')

@section('content')
	<div>
		<h2>{!! $test->questionnaire->name !!}, @lang('Askit::questionnaires.filledInBy') {!! $test->patient->fullName !!}</h2>

		<p>@lang('Askit::questionnaires.FilledInDate') {!! $test->created_at->format('d-m-Y') !!}</p>

	</div>
	<div>
		<table>
			<tr>
				<th style="text-align: left">@lang('Askit::questionnaires.Question')</th>
				<th style="text-align: left">@lang('Askit::questionnaires.Answer')</th>
			</tr>
			@foreach($questions as $item)
				<tr>
					<td>{!! $item->title !!}</td>
					<td>{!! $test->getAnswer($item)->humanReadableAnswer !!}</td>
				</tr>
			@endforeach
		</table>
	</div>
@endsection

@section('footer')
	{{ trans('progress.PdfWarning') }}
@endsection