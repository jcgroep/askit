@extends('export.pdf.master')

@section('content')
	<style>
		input{
			float: left;
		}

		textarea {
			width: 100%;
		}

	</style>
	<h2>{!! $questionnaire->name !!}</h2>
	{!! $questionnaireRenderer !!}
@endsection