<?php

use Jcgroep\Askit\DependencyOperator;

return [
    'createSuccess' => 'Nieuwe afhankelijkheid succesvol opgeslagen',
    'updateSuccess' => 'Afhankelijkheid succesvol gewijzigd',
    'deleteSuccess' => 'Afhankelijkheid succesvol verwijderd',
    'operators' => [
        DependencyOperator::SMALLER_THAN => 'Kleiner dan',
        DependencyOperator::SMALLER_THAN_OR_EQUAL => 'Kleiner dan of gelijk aan',
        DependencyOperator::EQUAL_TO => 'Gelijk aan',
        DependencyOperator::LARGER_THAN_OR_EQUAL => 'Groter dan of gelijk aan',
        DependencyOperator::LARGER_THAN => 'Groter dan',
        DependencyOperator::NOT_EQUAL_TO => 'Ongelijk aan',
    ],
];
