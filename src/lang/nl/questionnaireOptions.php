<?php

return [
    'createSuccess' => 'Nieuwe optie succesvol opgeslagen',
    'createSuccessCreateNewDirectly' => 'Nieuwe optie succesvol opgeslagen, u kunt direct nog een optie toevoegen.',
    'updateSuccess' => 'Optie succesvol gewijzigd',
    'deleteSuccess' => 'Optie succesvol verwijderd',
    'Create' => 'Voeg een nieuwe optie toe',
    'Edit' => 'Wijzig een bestaande optie',
    'createOther' => 'Maak nog een optie',
];
