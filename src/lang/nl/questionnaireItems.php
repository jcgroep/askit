<?php

return [
    'createSuccess' => 'Nieuwe vraag succesvol opgeslagen',
    'updateSuccess' => 'Vraag succesvol gewijzigd',
    'deleteSuccess' => 'Vraag succesvol verwijderd',
    'Questions' => 'Vragen',
    'duplicate' => 'Dupliceer vraag',
    'text' => 'Tekst velden',
    'multi' => 'Multiplechoice',
    'other' => 'Overig',
    'orientation' => 'Orientatie',
    'useOther' => 'Gebruik de anders namelijk optie',
    'defaultPlaceholder' => 'Voeg zelf een antwoord toe. Dit wordt automatisch opgeslagen',
    'showOptionValue' => 'Toon de waarde van de optie',
];