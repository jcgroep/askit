<?php

return [
    'close' => 'sluiten',
    'calculation' => 'Berekening',
    'required' => 'Verplicht',
    'postfix' => 'Postfix',
    'customCalculation' => 'Eigen berekening',
    'calculationType' => 'Type berekening',
    'show_total' => 'Toon dagtotaal',
    'validators' => 'Validatie',
    'validator_config1' => 'Waarde 1',
    'validator_config2' => 'Waarde 2',
    'validation' => [
        'min' => 'minimale waarde',
        'max' => 'maximale waarde',
    ],
];