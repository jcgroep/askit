<?php

use Jcgroep\Askit\DependencyOperator;

return [
    'createSuccess' => 'New dependency succesfully saved',
    'updateSuccess' => 'Dependency succesfully updated',
    'deleteSuccess' => 'Dependency succesfully deleted',
    'operators' => [
        DependencyOperator::SMALLER_THAN => 'Lower then',
        DependencyOperator::SMALLER_THAN_OR_EQUAL => 'Lower or equal to',
        DependencyOperator::EQUAL_TO => 'Equal to',
        DependencyOperator::LARGER_THAN_OR_EQUAL => 'Higher or equal to',
        DependencyOperator::LARGER_THAN => 'Higher then',
        DependencyOperator::NOT_EQUAL_TO => 'Not equal to',
    ],
];
