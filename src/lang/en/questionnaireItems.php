<?php

return [
    'createSuccess' => 'New question sucessfully saved',
    'updateSuccess' => 'Question succesfully updated',
    'deleteSuccess' => 'Question succesfully deleted',
    'Questions' => 'Questions',
    'duplicate' => 'Duplicate question',
    'text' => 'Text fields',
    'multi' => 'Multiplechoice',
    'other' => 'Other',
    'orientation' => 'Orientation',
    'useOther' => 'Use the \'other\' option',
];