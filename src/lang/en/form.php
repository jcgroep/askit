<?php

return [
    'close' => 'close',
    'calculation' => 'Calculation',
    'required' => 'Required',
    'postfix' => 'Postfix',
    'customCalculation' => 'Custom calculation',
    'calculationType' => 'Type of calculation',
    'show_total' => 'Show daily total',
    'validators' => 'Validation',
    'validator_config1' => 'Value 1',
    'validator_config2' => 'Value 2',
    'validation' => [
        'min' => 'minimum value',
        'max' => 'maximum value',
    ],
];