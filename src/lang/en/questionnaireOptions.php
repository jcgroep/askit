<?php

return [
    'createSuccess' => 'New option successfully saved',
    'createSuccessCreateNewDirectly' => 'New option successfully saved, you can continue with a new option',
    'updateSuccess' => 'Option successfully updated',
    'deleteSuccess' => 'Option successfully deleted',
    'Create' => 'Add a new option',
    'Edit' => 'Edit this option',
    'createOther' => 'Add another option',
];
